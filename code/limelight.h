#ifndef LIMELIGHT_H

#define VERSION_STRING CreateString("1.0.0")

enum subcommand_type
{
    SUBCOMMAND_TYPE_NONE  = 0,
    SUBCOMMAND_TYPE_BUILD = 1,
    SUBCOMMAND_TYPE_UPDATE = 2,
    SUBCOMMAND_TYPE_PUBLISH = 3,
    SUBCOMMAND_TYPE_INSTALL = 4,
};

#define LIMELIGHT_H
#endif
