#include "pkg-publish.h"
#include "build-file.h"
#include "pkg.h"

#include <dlib1/base.h>
#include <dlib1/stream.h>
#include <dlib1/commandline_parser.h>

static void CreateRepo(string ReposRoot, string RepoName);

void
PublishInit(commandline_parser* Parser)
{
    AddFlag(Parser, CreateString("r"), CreateString("repo"), ARGUMENT_TYPE_WORD,
            CreateString("Specifies the repo to use during the subcommand"));
    AddFlag(Parser, CreateString("e"), CreateString("create-repo"), ARGUMENT_TYPE_WORD,
            CreateString("Creates a new repo in the root directory"));
    AddFlag(Parser, CreateString("d"), CreateString("root"), ARGUMENT_TYPE_WORD,
            CreateString("The root directory to use if at all"));
    AddFlag(Parser, CreateString("f"), CreateString("build-file"), ARGUMENT_TYPE_WORD,
            CreateString("The special build file for the package manager"));
}

void
PublishRun(json* ConfigObject, commandline_parser* Parser)
{
    string ReposRoot      = CreateEmptyString();
    flag*  ReposRootFlag  = GetFlagByShortName(Parser, "d");
    flag*  CreateRepoFlag = GetFlagByShortName(Parser, "e");
    flag*  RepoFlag       = GetFlagByShortName(Parser, "r");
    flag*  BuildFileFlag  = GetFlagByShortName(Parser, "f");
    string RepoName       = CreateRepoFlag->Value;

    if (ConfigObject == NULL)
    {
        StandardPrint(
            CreateString("There is no configuration file please use -h for configuration options\n"));

        return;
    }

    GetJsonString(&ReposRoot, ConfigObject, "repos-root");

    if (ReposRoot.Length == 0 && ReposRootFlag->Found)
    {
        ReposRoot = ReposRootFlag->Value;
    }

    if (CreateRepoFlag->Found)
    {
        CreateRepo(ReposRoot, RepoName);

        return;
    }

    if (RepoFlag->Found)
    {
        RepoName = RepoFlag->Value;
    }
    else
    {
        StandardPrint(CreateString(
            "You must pass in a repo with the -r option use publish -h command for more information\n"));

        return;
    }

    array<package> PackageList;
    build_file     BuildFile;
    string         PackageListPath;
    string         EntireRepoPath;

    if (EndsWith(ReposRoot, CreateString("/")))
    {
        FormatString(&EntireRepoPath, "%str%str", ReposRoot, RepoName);
    }
    else
    {
        FormatString(&EntireRepoPath, "%str/%str", ReposRoot, RepoName);
    }

    CreateArray(&PackageList);
    FormatString(&PackageListPath, "%str/pkg.list", EntireRepoPath);

    if (DoesFileExist(PackageListPath))
    {
        file  PackageListFile       = OpenFile(PackageListPath, FILE_READ, false);
        u64   PackageListFileLength = GetFileLength(PackageListPath);
        char* PackageListFileBuffer = ReadFile(PackageListFile);

        ParsePackageList(&PackageList, PackageListFileBuffer, PackageListFileLength);
        FreeMemory(PackageListFileBuffer, PackageListFileLength);
        CloseSystemHandle(&PackageListFile.Handle);
        DeleteFile(PackageListPath);
    }

    if (ParseBuildFile(&BuildFile, BuildFileFlag->Value))
    {
        array<build>& NewBuilds = BuildFile.Builds;

        if (NewBuilds.Size != 1)
        {
            StandardPrint(CreateString("There should only be 1 build object in the build file\n"));

            WritePackageList(PackageListPath, &PackageList);
            FreeString(&PackageListPath);
            DestroyBuildFile(&BuildFile);
            return;
        }

        file DestinationBuildFile, SourceBuildFile = OpenFile(BuildFileFlag->Value, FILE_READ, false);

        if (SourceBuildFile.Handle < 0)
        {
            string ErrorMessage;

            FormatString(&ErrorMessage, "Failed to open source build file: %str\n", BuildFileFlag->Value);
            StandardPrint(ErrorMessage);
            FreeString(&ErrorMessage);

            WritePackageList(PackageListPath, &PackageList);
            FreeString(&PackageListPath);
            DestroyBuildFile(&BuildFile);
            return;
        }

        char* SourceBuffer       = ReadFile(SourceBuildFile);
        u64   SourceBufferLength = GetFileLength(BuildFileFlag->Value);

        for (u64 BuildIndex = 0; BuildIndex < NewBuilds.Size; ++BuildIndex)
        {
            build*   NewBuild   = NewBuilds + BuildIndex;
            package* NewPackage = Add(&PackageList);
            string   NewPackageName;
            string   NewPackageVersion;

            if (!GetJsonString(&NewPackageName, NewBuild->BuildObject, "package-name"))
            {
                string ErrorMessage;

                FormatString(&ErrorMessage, "Could not get package-name from build: %str\n",
                             NewBuild->BuildObject->Name);
                StandardPrint(ErrorMessage);
                FreeString(&ErrorMessage);

                FreeMemory(SourceBuffer, SourceBufferLength);
                WritePackageList(PackageListPath, &PackageList);
                FreeString(&PackageListPath);
                DestroyBuildFile(&BuildFile);
                return;
            }

            if (!GetJsonString(&NewPackageVersion, NewBuild->BuildObject, "package-version"))
            {
                string ErrorMessage;

                FormatString(&ErrorMessage, "Could not get package-version from build: %str\n",
                             NewBuild->BuildObject->Name);
                StandardPrint(ErrorMessage);
                FreeString(&ErrorMessage);

                FreeMemory(SourceBuffer, SourceBufferLength);
                WritePackageList(PackageListPath, &PackageList);
                FreeString(&PackageListPath);
                DestroyBuildFile(&BuildFile);
                return;
            }

            string BuildFileName;

            CopyString(&NewPackage->Name, NewPackageName);
            CopyString(&NewPackage->Version, NewPackageVersion);
            FormatString(&BuildFileName, "%str/%str-%str.json", EntireRepoPath, NewPackage->Name,
                         NewPackage->Version);

            if (DoesFileExist(BuildFileName))
            {
                DeleteFile(BuildFileName);
            }

            DestinationBuildFile = OpenFile(BuildFileName, FILE_WRITE, true);

            string Message;

            FormatString(&Message, "Publishing %str-%str\n", NewPackage->Name, NewPackage->Version);
            StandardPrint(Message);
            FreeString(&Message);

            if (DestinationBuildFile.Handle < 0)
            {
                string ErrorMessage;

                FormatString(&ErrorMessage, "Failed to open destination build file: %str\n", BuildFileName);
                StandardPrint(ErrorMessage);
                FreeString(&ErrorMessage);

                FreeString(&BuildFileName);
                FreeMemory(SourceBuffer, SourceBufferLength);
                WritePackageList(PackageListPath, &PackageList);
                FreeString(&PackageListPath);
                DestroyBuildFile(&BuildFile);
                return;
            }

            WriteFile(DestinationBuildFile, SourceBuffer, SourceBufferLength);

            FreeString(&BuildFileName);
            CloseSystemHandle(&DestinationBuildFile.Handle);
        }

        FreeMemory(SourceBuffer, SourceBufferLength);
        CloseSystemHandle(&SourceBuildFile.Handle);
        DestroyBuildFile(&BuildFile);
    } // Error messaging is handled inside ParseBuildFile

    {
        string Message;

        FormatString(&Message, "Updating Package List: %str\n", PackageListPath);
        StandardPrint(Message);
        FreeString(&Message);
    }

    FreeString(&EntireRepoPath);
    WritePackageList(PackageListPath, &PackageList);
    FreeString(&PackageListPath);
    FreePackageList(&PackageList);
    FreeArray(&PackageList);
    StandardPrint(CreateString("Publishing complete.\n"));

    return;
}

static void
CreateRepo(string ReposRoot, string RepoName)
{
    if (ReposRoot.Length == 0)
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "Failed to create new repo: %str, root repos directory unset\n",
                     RepoName);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        return;
    }

    string EntireRepoPath, Message;

    if (EndsWith(ReposRoot, CreateString("/")))
    {
        FormatString(&EntireRepoPath, "%str%str", ReposRoot, RepoName);
    }
    else
    {
        FormatString(&EntireRepoPath, "%str/%str", ReposRoot, RepoName);
    }

    if (!MakeDirectory(EntireRepoPath))
    {
        FormatString(&Message, "Could not make repo at: %str\n", EntireRepoPath);
    }
    else
    {
        FormatString(&Message, "Created New Repository: %str\n", RepoName);
    }

    StandardPrint(Message);
    FreeString(&Message);
}
