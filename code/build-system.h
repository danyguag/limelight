#if !defined(BUILD_SYSTEM_H)

#include <dlib1/commandline_parser.h>

void BuildInit(commandline_parser* Parser);
void BuildRun(commandline_parser* Parser);

#define BUILD_SYSTEM_H
#endif
