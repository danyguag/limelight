#if !defined(URL_FETCHER_H)

#include <curl/curl.h>
#include <dlib1/stream.h>
#include <dlib1/string.h>

CURL* InitCurl(stream* Stream);
void  DestroyCurl(CURL* Curl, stream* Stream);
b32   GetUrlRequest(CURL* Curl, stream* Stream, string Url);

#define URL_FETCHER_H
#endif
