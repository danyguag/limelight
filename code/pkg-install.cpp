#include "pkg-install.h"
#include "build.h"
#include "pkg.h"
#include "platform.h"
#include "url-fetcher.h"

#include <dlib1/base.h>
#include <dlib1/string.h>
#include <dlib1/timer.h>

b32 FindRemoteForPackageFromLocalStorage(string* Name, string* Repo, string* RepoUrl, string PackageName,
                                         string PackageVersion, array<json>* SourceObjects);

void
InstallInit(commandline_parser* Parser)
{
    AddFlag(Parser, CreateString("n"), CreateString("package-name"), ARGUMENT_TYPE_WORD,
            CreateString("The name of the package to install"));
    AddFlag(Parser, CreateString("e"), CreateString("package-version"), ARGUMENT_TYPE_WORD,
            CreateString("The version of the package to install"));
}

void
InstallRun(json* ConfigFileObject, commandline_parser* Parser)
{
    if (ConfigFileObject == NULL)
    {
        StandardPrint(
            CreateString("There is no configuration file please use -h for configuration options\n"));

        return;
    }

    flag*       PackageNameFlag       = GetFlagByShortName(Parser, "n");
    flag*       PackageVersionFlag    = GetFlagByShortName(Parser, "e");
    u8          InstalledPackageCount = 0;
    array<json> SourceObjects;

    GetJsonObjectArray(&SourceObjects, ConfigFileObject, "sources");

    if (SourceObjects.Size == 0)
    {
        StandardPrint(CreateString("Could not read sources from configuration\n"));

        return;
    }

    if (!PackageNameFlag->Found)
    {
        StandardPrint(
            CreateString("You must pass in -n or --package-name to specify a package to install\n"));

        return;
    }

    if (!PackageNameFlag->Found)
    {
        StandardPrint(CreateString(
            "You must pass in -e or --package-version to specify the package version to install\n"));

        return;
    }

    string PackageName    = PackageNameFlag->Value;
    string PackageVersion = PackageVersionFlag->Value;
    string RepoName;
    string Repo;
    string RepoUrl;

    if (!FindRemoteForPackageFromLocalStorage(&RepoName, &Repo, &RepoUrl, PackageName, PackageVersion,
                                              &SourceObjects))
    {
        string ErrorMessage;

        FormatString(
            &ErrorMessage,
            "Could not find %str-%str from existing package lists, if the packages exists try updating first\n",
            PackageName, PackageVersion);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);
    }

    stream* Stream = (stream*)AllocateMemory(sizeof(stream));
    CURL*   Curl   = InitCurl(Stream);
    string  FullUrl;

    FormatString(&FullUrl, "%str%str/%str-%str.json", RepoUrl, Repo, PackageName, PackageVersion);

    if (!GetUrlRequest(Curl, Stream, FullUrl))
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "FAILED: could not connect: %str\n", FullUrl);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        FreeString(&FullUrl);
        DestroyCurl(Curl, Stream);
        FreeMemory((char*)Stream, sizeof(stream));

        return;
    }

    FreeString(&FullUrl);
    FreeString(&RepoName);
    FreeString(&RepoUrl);
    FreeString(&Repo);

    json BuildFileObject;
    engine_memset((char*)&BuildFileObject, 0, sizeof(json));

    BufferToJSON(&BuildFileObject, Stream->Buffer, Stream->WritePointer);

    array<json>* BuildObjects = &BuildFileObject.Objects;
    array<build> Builds;
    string       PreviousWorkingDirectory;

    CreateArray(&Builds);
    GetCurrentWorkingDirectory(&PreviousWorkingDirectory);

    {
        string Message;

        FormatString(&Message, "Installing: %str-%str\n", PackageName, PackageVersion);
        StandardPrint(Message);
        FreeString(&Message);
    }

    for (u64 BuildIndex = 0; BuildIndex < BuildObjects->Size; ++BuildIndex)
    {
        json*  BuildObject = GetElement(BuildObjects, BuildIndex);
        string SourceType;
        string SourceUrl;
        string FullPackageDirectoryName;

        if (!GetJsonString(&SourceType, BuildObject, "source-type"))
        {
            string Message;

            FormatString(&Message, "Missing source-type from install build file for package: %str-%str\n",
                         PackageName, PackageVersion);
            StandardPrint(Message);
            FreeString(&Message);

            continue;
        }

        if (!GetJsonString(&SourceUrl, BuildObject, "source-url"))
        {
            string Message;

            FormatString(&Message, "Missing source-url from install build file for package: %str-%str\n",
                         PackageName, PackageVersion);
            StandardPrint(Message);
            FreeString(&Message);

            continue;
        }

        FormatString(&FullPackageDirectoryName, "/tmp/%str-%str-%s64", PackageName, PackageVersion, GetTime());

        if (SourceType == "git")
        {
            string        GitPath = CreateString("/usr/bin/git");
            array<string> Flags;

            CreateArray(&Flags);
            Add(&Flags, CreateString("clone"));
            Add(&Flags, CreateString("-q"));
            Add(&Flags, SourceUrl);
            Add(&Flags, FullPackageDirectoryName);

            CallExecutable(GitPath, &Flags);

            for (u64 FlagIndex = 0; FlagIndex < Flags.Size; ++FlagIndex)
            {
                FreeString(Flags + FlagIndex);
            }

            FreeArray(&Flags);
        }

        string SourceBuildDirectory;
        string SourceCodeDirectory;

        FormatString(&SourceBuildDirectory, "%str/build", FullPackageDirectoryName);
        FormatString(&SourceCodeDirectory, "%str/code", FullPackageDirectoryName);
	FreeString(&FullPackageDirectoryName);
	
        if (!DoesFileExist(SourceBuildDirectory) && !MakeDirectory(SourceBuildDirectory))
        {
            string Message;

            FormatString(&Message, "Invalid build directory: %str\n", SourceBuildDirectory);
            StandardPrint(Message);
            FreeString(&Message);

            continue;
        }

        if (!DoesFileExist(SourceCodeDirectory))
        {
            string Message;

            FormatString(&Message, "Invalid code directory: %str\n", SourceBuildDirectory);
            StandardPrint(Message);
            FreeString(&Message);

            continue;
        }

        array<json_string>* Strings = &BuildObject->Strings;

        for (u64 StringIndex = 0; StringIndex < Strings->Size; ++StringIndex)
        {
            json_string* String = GetElement(Strings, StringIndex);

            if (String->Name == "output_directory")
            {
                FreeString(&String->Value);
                engine_memcpy((char*)&String->Value, &SourceBuildDirectory, sizeof(string));

                break;
            }
        }

        build* NewBuild = Add(&Builds);

        NewBuild->BuildObject = BuildObject;
        NewBuild->BaseFolder  = SourceCodeDirectory;
    }

    ExecuteBuilds(&Builds, BUILD_OPTION_RELEASE);
    SetWorkingDirectory(PreviousWorkingDirectory);
    DestroyCurl(Curl, Stream);
    FreeMemory((char*)Stream, sizeof(stream));

    for (u64 BuildIndex = 0; BuildIndex < BuildObjects->Size; ++BuildIndex)
    {
        build* CurrentBuild = Builds + BuildIndex;

        if (CurrentBuild->Result && CurrentBuild->OutputType == OUTPUT_TYPE_EXECUTABLE)
        {
            string FullExecutableName;
            string DestinationPath;

            FormatString(&FullExecutableName, "%str/%str", CurrentBuild->OutputDirectory,
                         CurrentBuild->ExecutableName);
            FormatString(&DestinationPath, "%str/%str-%str", PreviousWorkingDirectory, PackageName,
                         PackageVersion);

            if (!CopyFile(FullExecutableName, DestinationPath))
            {
                string ErrorMessage;

                FormatString(&ErrorMessage, "Failed to install %str-%str to %str\n", PackageName,
                             PackageVersion, DestinationPath);
                StandardPrint(ErrorMessage);
                FreeString(&ErrorMessage);
            }
            else
            {
                string ErrorMessage;

                FormatString(&ErrorMessage, "Installed %str-%str to %str\n", PackageName, PackageVersion,
                             DestinationPath);
                StandardPrint(ErrorMessage);
                FreeString(&ErrorMessage);

                ++InstalledPackageCount;
                SetExecutable(DestinationPath);
                FreeString(&FullExecutableName);
                FreeString(&DestinationPath);
            }
        }
        else if (CurrentBuild->Result && (CurrentBuild->OutputDirectory == OUTPUT_TYPE_SHARED ||
                                          CurrentBuild->OutputDirectory == OUTPUT_TYPE_STATIC))
        {
            StandardPrint(CreateString("Installing newly built libraries is not supported\n"));
        }
	
        FreeString(&CurrentBuild->BaseFolder);
    }

    FreeString(&PreviousWorkingDirectory);
    DestroyBuilds(&Builds);
    FreeArray(&Builds);
    DestroyJson(&BuildFileObject);

    if (InstalledPackageCount == 0)
    {
        string Message;

        FormatString(&Message, "Failed to install package: %str-%str\n", PackageName, PackageVersion);
        StandardPrint(Message);
        FreeString(&Message);
    }
}

b32
FindRemoteForPackageFromLocalStorage(string* pName, string* pRepo, string* pRepoUrl, string PackageName,
                                     string PackageVersion, array<json>* SourceObjects)
{
    string UserHomeDirectory;
    string StoragePath;

    GetUserHomeDirectory(&UserHomeDirectory);
    FormatString(&StoragePath, "%str/.local/limelight/", UserHomeDirectory);

    if (!DoesFileExist(StoragePath) && !SetWorkingDirectory(StoragePath))
    {
        StandardPrint(CreateString("Make sure you have called update before trying to install\n"));

        return (false);
    }

    b32              Found = false;
    array<io_object> Files = SearchFolder(StoragePath);
    array<package>   PackageList;

    CreateArray(&PackageList);

    for (u64 FileIndex = 0; FileIndex < Files.Size; ++FileIndex)
    {
        io_object* File = Files + FileIndex;

        if (File->Type != IO_FILE)
        {
            FreeString(&File->Name);
            continue;
        }

        s64 DashIndex = IndexOf(File->Name, '-');

        if (DashIndex == -1)
        {
            FreeString(&File->Name);
            continue;
        }

        string RepoName;
        string Repo;
        string RepoUrl          = CreateEmptyString();
        string RepoFileNamePart = File->Name[(u64)DashIndex + 1];

        SubString(&RepoName, File->Name, 0, (u64)DashIndex);

        s64 SecondDashIndex = IndexOf(RepoFileNamePart, '-');

        if (SecondDashIndex == -1)
        {
            FreeString(&RepoName);
            FreeString(&File->Name);
            continue;
        }

        SubString(&Repo, RepoFileNamePart, 0, (u64)SecondDashIndex);

        for (u64 SourceIndex = 0; SourceIndex < SourceObjects->Size; ++SourceIndex)
        {
            json*  SourceObject = GetElement(SourceObjects, SourceIndex);
            string Name, Url;

            if (!GetJsonString(&Name, SourceObject, "name"))
            {
                StandardPrint(CreateString("Invalid source object\n"));

                continue;
            }

            if (!GetJsonString(&Url, SourceObject, "url"))
            {
                StandardPrint(CreateString("Invalid source object\n"));

                continue;
            }

            if (Name == RepoName)
            {
                // TODO: check to ensure the repo exists
                CopyString(&RepoUrl, Url);
                break;
            }
        }

        if (RepoUrl.Length == 0)
        {
            string ErrorMessage;

            FormatString(&ErrorMessage, "Could not find a package source with name: %str\n", RepoName);
            StandardPrint(ErrorMessage);
            FreeString(&ErrorMessage);

            continue;
        }

        string FullFileName;

        FormatString(&FullFileName, "%str%str", StoragePath, File->Name);
        file PackageFile = OpenFile(FullFileName, FILE_READ, false);

        if (PackageFile.Handle < 0)
        {
            string Message;

            FormatString(&Message, "Failed to open package list file: %str\n", FullFileName);
            StandardPrint(Message);

            FreeString(&FullFileName);
            FreeString(&Message);
            FreeString(&RepoName);
            FreeString(&RepoUrl);
            FreeString(&Repo);
            FreeString(&File->Name);

            continue;
        }

        char* PackageFileBuffer = ReadFile(PackageFile);
        u64   PackageFileLength = GetFileLength(File->Name);

        ParsePackageList(&PackageList, PackageFileBuffer, PackageFileLength);

        for (u64 PackageIndex = 0; PackageIndex < PackageList.Size; ++PackageIndex)
        {
            package* Package = PackageList + PackageIndex;

            if (Package->Name == PackageName && Package->Version == PackageVersion)
            {
                CopyString(pName, RepoName);
                CopyString(pRepo, Repo);
                CopyString(pRepoUrl, RepoUrl);

                Found = true;
            }
        }

        FreeString(&RepoName);
        FreeString(&RepoUrl);
        FreeString(&Repo);
        FreePackageList(&PackageList);
        FreeMemory(PackageFileBuffer, PackageFileLength);
        FreeString(&FullFileName);
        CloseSystemHandle(&PackageFile.Handle);
        ClearArray(&PackageList);
        FreeString(&File->Name);

        if (Found)
        {
            break;
        }
    }

    FreeString(&StoragePath);
    FreeArray(&PackageList);
    FreeArray(&Files);

    return (Found);
}
