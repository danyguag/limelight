#ifndef PUBLISH_H

#include <dlib1/commandline_parser.h>
#include <dlib1/json.h>

void PublishInit(commandline_parser* Parser);
void PublishRun(json* ConfigObject, commandline_parser* Parser);

#define PUBLISH_H
#endif
