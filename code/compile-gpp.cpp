#include "compile-gpp.h"
#include "compile-ar.h"
#include "platform.h"

#include <dlib1/base.h>

DESTROY_COMPILATION_USERDATA(DestroyCompilationUserDataGpp);
CREATE_LIBRARY_STATIC(CreateLibraryStaticGpp);
CREATE_LIBRARY_SHARED(CreateLibrarySharedGpp);
CREATE_EXECUTABLE(CreateExecutableGpp);

static void CreateBaseFlags(array<string>* BaseFlags, build_type BuildType);

struct gpp_build
{
    array<string> BaseFlags;
    array<string> ObjectFileNames;
};

void
GetGppCompilationProcess(build* Build)
{
    gpp_build* BuildData = (gpp_build*)AllocateMemory(sizeof(gpp_build));

    CreateBaseFlags(&BuildData->BaseFlags, Build->BuildType);
    CreateArray(&BuildData->ObjectFileNames);

    Build->DestroyCompilationUserData = DestroyCompilationUserDataGpp;
    Build->CreateLibraryStatic        = CreateLibraryStaticGpp;
    Build->CreateLibraryShared        = CreateLibrarySharedGpp;
    Build->CreateExecutable           = CreateExecutableGpp;
    Build->UserData                   = BuildData;
}

void
DestroyCompilationUserDataGpp(void* UserData)
{
    gpp_build* BuildData = (gpp_build*)UserData;

    for (u64 BaseFlagIndex = 0; BaseFlagIndex < BuildData->BaseFlags.Size; ++BaseFlagIndex)
    {
        FreeString(BuildData->BaseFlags + BaseFlagIndex);
    }

    for (u64 ObjectFileNameIndex = 0; ObjectFileNameIndex < BuildData->ObjectFileNames.Size;
         ++ObjectFileNameIndex)
    {
        FreeString(BuildData->ObjectFileNames + ObjectFileNameIndex);
    }

    FreeArray(&BuildData->BaseFlags);
    FreeArray(&BuildData->ObjectFileNames);
    FreeMemory((char*)UserData, sizeof(gpp_build));
}

b32
CreateCompilationObjects(build* Build, gpp_build* BuildData)
{
    array<array<string>> ObjectFileArgs;
    string               CompilerPath             = CreateString("/usr/bin/g++");
    u32                  InputFileCount           = Build->InputFiles.Size;
    u64                  BuildObjectFileNameCount = BuildData->ObjectFileNames.Size;

    CreateArray(&ObjectFileArgs);

    for (u32 FileIndex = 0; FileIndex < InputFileCount; ++FileIndex)
    {
        comp_file* CurrentBuildFile = Build->InputFiles + FileIndex;
        string     ObjectFileName   = CurrentBuildFile->ObjectFileName;

        array<string> CurrentObjectFileArgs;
        CreateArray(&CurrentObjectFileArgs);
        Add(&CurrentObjectFileArgs, &BuildData->BaseFlags);
        Add(&CurrentObjectFileArgs, CreateString("-c"));
        Add(&CurrentObjectFileArgs, CurrentBuildFile->FullName);
        Add(&CurrentObjectFileArgs, CreateString("-fPIC"));
        Add(&CurrentObjectFileArgs, CreateString("-lpthread"));

        if (Build->ExtraFlags.AllocatedLength > 0)
        {
            Add(&CurrentObjectFileArgs, &Build->ExtraFlags);
        }

        if (Build->Dependencies.IncludeStrings.AllocatedLength > 0)
        {
            Add(&CurrentObjectFileArgs, &Build->Dependencies.IncludeStrings);
        }

        GenerateCompileCommand(&CurrentBuildFile->Command, CompilerPath, &CurrentObjectFileArgs);

        Add(&BuildData->ObjectFileNames, ObjectFileName);
        Add(&ObjectFileArgs, CurrentObjectFileArgs);
    }

    // Execute all of the commands at one time on different threads
    CallExecutablesAsync(CompilerPath, &ObjectFileArgs, true);

    b32 Result = true;

    for (u64 CommandIndex = 0; CommandIndex < ObjectFileArgs.Size; ++CommandIndex)
    {
        array<string>* Args = ObjectFileArgs + CommandIndex;

        for (u64 ArgIndex = 0; ArgIndex < Args->Size; ++ArgIndex)
        {
            FreeString(GetElement(Args, ArgIndex));
        }

        FreeArray(Args);
    }

    for (u64 ObjectFileNameIndex = BuildObjectFileNameCount;
         ObjectFileNameIndex < BuildData->ObjectFileNames.Size; ++ObjectFileNameIndex)
    {
        string& ObjectFileName = BuildData->ObjectFileNames[ObjectFileNameIndex];

        if (!DoesFileExist(ObjectFileName))
        {
            Result = false;
            break;
        }
    }

    FreeArray(&ObjectFileArgs);

    return (Result);
}

b32
CreateLibraryStaticGpp(build* Build, void* UserData)
{
    gpp_build* BuildData = (gpp_build*)UserData;

    if (!CreateCompilationObjects(Build, BuildData))
    {
        return (false);
    }

    if (Build->Dependencies.LibraryStrings.AllocatedLength > 0)
    {
        Add(&BuildData->ObjectFileNames, &Build->Dependencies.LibraryStrings);
    }

    string LibraryName;

    FormatString(&LibraryName, "lib%str.a", Build->ExecutableName);
    CreateStaticLibraryAr(LibraryName, NULL, &BuildData->ObjectFileNames);

    b32 Result = DoesFileExist(LibraryName);

    FreeString(&LibraryName);

    return (Result);
}

b32
CreateLibrarySharedGpp(build* Build, void* UserData)
{
    array<string> Flags;
    gpp_build*    BuildData    = (gpp_build*)UserData;
    string        CompilerPath = CreateString("/usr/bin/g++");

    if (!CreateCompilationObjects(Build, BuildData))
    {
        return (false);
    }

    CreateArray(&Flags);
    Add(&Flags, CreateString("-pthread"));
    Add(&Flags, CreateString("-shared"));
    Add(&Flags, CreateString("-o"));

    string* LibraryName = Add(&Flags);

    FormatString(LibraryName, "lib%str.so", Build->ExecutableName);

    if (Build->Dependencies.LibraryStrings.AllocatedLength > 0)
    {
        Add(&Flags, &Build->Dependencies.LibraryStrings);
    }

    Add(&Flags, &BuildData->ObjectFileNames);

    if (Build->ExtraFlags.AllocatedLength > 0)
    {
        Add(&Flags, &Build->ExtraFlags);
    }

    Add(&Flags, CreateString("-lpthread"));
    CallExecutable(CompilerPath, &Flags);
    b32 Result = DoesFileExist(*LibraryName);

    for (u64 FlagIndex = 0; FlagIndex < Flags.Size; ++FlagIndex)
    {
        FreeString(Flags + FlagIndex);
    }

    FreeArray(&Flags);

    return (Result);
}

b32
CreateExecutableGpp(build* Build, void* UserData)
{
    array<string> Flags;
    gpp_build*    BuildData    = (gpp_build*)UserData;
    string        CompilerPath = CreateString("/usr/bin/g++");

    if (Build->InputFiles.Size > 0 && !CreateCompilationObjects(Build, BuildData))
    {
        return (false);
    }

    CreateArray(&Flags);
    Add(&Flags, &BuildData->BaseFlags);
    Add(&Flags, CreateString("-o"));
    Add(&Flags, Build->ExecutableName);
    Add(&Flags, Build->InputFile.FullName);

    if (Build->Dependencies.IncludeStrings.AllocatedLength > 0)
    {
        Add(&Flags, &Build->Dependencies.IncludeStrings);
    }

    if (Build->InputFiles.Size > 0)
    {
        Add(&Flags, &BuildData->ObjectFileNames);
    }

    if (Build->Dependencies.LibraryStrings.AllocatedLength > 0)
    {
        Add(&Flags, &Build->Dependencies.LibraryStrings);
    }

    Add(&Flags, CreateString("-lpthread"));
    if (Build->ExtraFlags.AllocatedLength > 0)
    {
        Add(&Flags, &Build->ExtraFlags);
    }

    GenerateCompileCommand(&Build->InputFile.Command, CompilerPath, &Flags);

    CallExecutable(CompilerPath, &Flags);

    for (u64 FlagIndex = 0; FlagIndex < Flags.Size; ++FlagIndex)
    {
	FreeString(Flags + FlagIndex);
    }

    FreeArray(&Flags);
    
    return DoesFileExist(Build->ExecutableName);
}

static void
CreateBaseFlags(array<string>* BaseFlags, build_type BuildType)
{
    CreateArray(BaseFlags);

    Add(BaseFlags, CreateString("-pthread"));
    Add(BaseFlags, CreateString("-Wall"));
    Add(BaseFlags, CreateString("-Werror"));
    Add(BaseFlags, CreateString("-Wextra"));
    Add(BaseFlags, CreateString("-std=c++17"));
    Add(BaseFlags, CreateString("-Wno-write-strings"));
    Add(BaseFlags, CreateString("-Wno-unused-function"));
    Add(BaseFlags, CreateString("-fno-exceptions"));
    Add(BaseFlags, CreateString("-Wno-unused-result"));
    Add(BaseFlags, CreateString("-DPLATFORM_LINUX"));

    if (BuildType == BUILD_TYPE_DEBUG)
    {
        Add(BaseFlags, CreateString("-g"));
        Add(BaseFlags, CreateString("-O0"));
    }
    else if (BuildType == BUILD_TYPE_RELEASE)
    {
        Add(BaseFlags, CreateString("-O3"));
    }
}
