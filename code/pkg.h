#ifndef PKG_H

#include <dlib1/array.h>
#include <dlib1/commandline_parser.h>
#include <dlib1/json.h>
#include <dlib1/string.h>

struct package
{
    string Name;
    string Version;
};

void ParsePackageList(array<package>* PackageList, char* Buffer, u64 Length);
void WritePackageList(string PackageListPath, array<package>* PackageList);
void FreePackageList(array<package>* PackageList);

void UpdateInit(commandline_parser* Parser);
void UpdateRun(json* Object, commandline_parser* Parser);

#define PKG_H
#endif
