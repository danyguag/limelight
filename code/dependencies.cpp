#include "dependencies.h"

#include <dlib1/stream.h>
#include <dlib1/string.h>

void
BuildDependencies(dependencies* Dependencies, array<string>* IncludeFolders, array<string>* LibraryPaths)
{
    // Add the -I to the include path and then add it to the include strings
    if (IncludeFolders && IncludeFolders->Size > 0)
    {
        u32 IncludeFolderCount = IncludeFolders->Size;

        CreateArray(&Dependencies->IncludeStrings);

        for (u32 IncludeFolderIndex = 0; IncludeFolderIndex < IncludeFolderCount; ++IncludeFolderIndex)
        {
            string  IncludeFolder       = *GetElement(IncludeFolders, IncludeFolderIndex);
            string* IncludeFolderString = Add(&Dependencies->IncludeStrings);

            FormatString(IncludeFolderString, "-I%str", IncludeFolder);
        }
    }

    // Just copy the library array to the one that will be used in the build process
    if (LibraryPaths && LibraryPaths->Size > 0)
    {
        CopyArray(&Dependencies->LibraryStrings, LibraryPaths);
    }
}
