#include "build-system.h"
#include "build-file.h"

void
BuildInit(commandline_parser* Parser)
{
    AddFlag(
        Parser, CreateString("r"), CreateString("release"), ARGUMENT_TYPE_FLAG,
        CreateString(
            "If this is passed the build and all following build hooks will be forced to compiled in release"));
    AddFlag(
        Parser, CreateString("d"), CreateString("debug"), ARGUMENT_TYPE_FLAG,
        CreateString(
            "If this is passed the build and all following build hooks will be forced to compiled in release"));
    AddFlag(Parser, CreateString("f"), CreateString("build-file"), ARGUMENT_TYPE_WORD,
            CreateString("Specifies a build file"));
    AddFlag(Parser, CreateString("g"), CreateString("generate"), ARGUMENT_TYPE_WORD,
            CreateString("Generates a compile commands for rtags in json format"));
    AddFlag(Parser, CreateString("i"), CreateString("install"), ARGUMENT_TYPE_FLAG,
            CreateString("Installs any builds with the configuration to install"));
    AddFlag(Parser, CreateString("s"), CreateString("shared"), ARGUMENT_TYPE_WORD,
            CreateString("If this is passed the build will be forced to build a shared library"));
    AddFlag(Parser, CreateString("t"), CreateString("static"), ARGUMENT_TYPE_FLAG,
            CreateString("If this is passed the build will be forced to build a static library"));
}

void
BuildRun(commandline_parser* Parser)
{
    // Build the build system structure
    build_file BuildFile;
    string     BuildFileName = CreateString("build.json");
    u32        FlagBits      = 0;

    if (GetFlagByShortName(Parser, "r")->Found)
    {
        FlagBits |= BUILD_OPTION_RELEASE;
    }

    if (GetFlagByShortName(Parser, "d")->Found)
    {
        FlagBits |= BUILD_OPTION_DEBUG;
    }

    if (GetFlagByShortName(Parser, "g")->Found)
    {
        FlagBits |= BUILD_OPTION_GENERATE_COMPILE_COMMANDS;
    }

    if (GetFlagByShortName(Parser, "s")->Found)
    {
        FlagBits |= BUILD_OPTION_SHARED;
    }
    else if (GetFlagByShortName(Parser, "t")->Found)
    {
        FlagBits |= BUILD_OPTION_STATIC;
    }

    if (GetFlagByShortName(Parser, "i")->Found)
    {
        FlagBits |= BUILD_OPTION_INSTALL;
    }

    flag* BuildFileFlag = GetFlagByShortName(Parser, "f");

    if (BuildFileFlag->Found)
    {
        BuildFileName = BuildFileFlag->Value;
    }

    if (ParseBuildFile(&BuildFile, BuildFileName))
    {
        ExecuteBuilds(&BuildFile.Builds, FlagBits);

        DestroyBuildFile(&BuildFile);
    }
}
