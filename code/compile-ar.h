#if !defined(COMPILE_AR_H)

#include "build.h"

void CreateStaticLibraryAr(string PrettyLibraryName, array<string>* LinkerFlags, array<string>* ObjectFileNames);

#define COMPILE_AR_H
#endif
