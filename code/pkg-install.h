#if !defined(PKG_INSTALL_H)

#include <dlib1/commandline_parser.h>
#include <dlib1/json.h>

void InstallInit(commandline_parser* Parser);
void InstallRun(json* ConfigObject, commandline_parser* Parser);

#define PKG_INSTALL_H
#endif
