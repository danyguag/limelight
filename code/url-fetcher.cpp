#include "url-fetcher.h"

size_t CurlWriteCallback(char* Source, size_t Size, size_t BytesToParse, void* Memory);

CURL*
InitCurl(stream* Stream)
{
    CURL* Curl = curl_easy_init();

    CreateStream(Stream, Megabytes(8), HEAP_ALLOCATOR_MEMORY_CALLBACK());
    curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, CurlWriteCallback);
    curl_easy_setopt(Curl, CURLOPT_WRITEDATA, Stream);

    return Curl;
}

void
DestroyCurl(CURL* Curl, stream* Stream)
{
    DestroyStream(Stream);
    curl_easy_cleanup(Curl);
}

b32
GetUrlRequest(CURL* Curl, stream* Stream, string Url)
{
    curl_easy_setopt(Curl, CURLOPT_URL, Url.Buffer);

    ResetStream(Stream);

    b32 Result = curl_easy_perform(Curl) == CURLE_OK;

    return (Result && Stream->WritePointer > 0);
}

size_t
CurlWriteCallback(char* Source, size_t Size, size_t BytesToParse, void* Memory)
{
    Write((stream*)Memory, Source, BytesToParse);

    UNUSED_VARIABLE(Size);
    return (BytesToParse);
}
