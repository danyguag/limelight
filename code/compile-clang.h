#if !defined(COMPILE_CLANG_H)

#include "build.h"

void GetClangCompilationProcess(build* Build);

#define COMPILE_CLANG_H
#endif
