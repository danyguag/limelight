#if !defined(DEPENDENCIES_H)

#include <dlib1/array.h>

struct dependencies
{
    array<string> IncludeStrings;
    array<string> LibraryStrings;
};

void BuildDependencies(dependencies* Dependencies, array<string>* IncludeFolders, array<string>* LibraryPaths);

#define DEPENDENCIES_H
#endif
