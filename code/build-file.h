#if !defined(BUILD_FILE_H)

#include <dlib1/array.h>
#include <dlib1/json.h>

#include "build.h"

struct build_file
{
    json BuildFileObject;
    array<build> Builds;
    string InitialDirectory;
};

b32 ParseBuildFile(build_file* BuildFile, string BuildFileName);
void DestroyBuildFile(build_file* BuildFile);

#define BUILD_FILE_H
#endif
