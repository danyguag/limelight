#include "pkg.h"
#include "url-fetcher.h"
#include "platform.h"

#include <curl/curl.h>
#include <dlib1/base.h>
#include <dlib1/commandline_parser.h>

static void UpdateSource(json* SourceObject, CURL* Curl, stream* Stream, string StoragePath);

void
ParsePackageList(array<package>* PackageList, char* Buffer, u64 Length)
{
    stream ReadStream;
    SetBuffer(&ReadStream, Buffer, Length);
    ReadStream.ReadPointer = 0;

    u64 PackageCount = Read<u64>(&ReadStream);

    for (u64 PackageIndex = 0; PackageIndex < PackageCount; ++PackageIndex)
    {
        package* NewPackage = Add(PackageList);

        u64    NewPackageNameLength = Read<u64>(&ReadStream);
        string NewPackageName =
            CreateString((char*)Read(&ReadStream, NewPackageNameLength), NewPackageNameLength);
        u64    NewPackageVersionLength = Read<u64>(&ReadStream);
        string NewPackageVersion =
            CreateString((char*)Read(&ReadStream, NewPackageVersionLength), NewPackageVersionLength);

        CopyString(&NewPackage->Name, NewPackageName);
        CopyString(&NewPackage->Version, NewPackageVersion);
    }
}

void
WritePackageList(string PackageListPath, array<package>* PackageList)
{
    file   PackageListFile = OpenFile(PackageListPath, FILE_WRITE, true);
    stream WriteStream;

    CreateStream(&WriteStream);

    Write(&WriteStream, PackageList->Size);

    for (u64 PackageIndex = 0; PackageIndex < PackageList->Size; ++PackageIndex)
    {
        package* Package = GetElement(PackageList, PackageIndex);

        Write(&WriteStream, Package->Name.Length);
        Write(&WriteStream, Package->Name.Buffer, Package->Name.Length);
        Write(&WriteStream, Package->Version.Length);
        Write(&WriteStream, Package->Version.Buffer, Package->Version.Length);
    }

    WriteFile(PackageListFile, WriteStream.Buffer, WriteStream.WritePointer);
    CloseSystemHandle(&PackageListFile.Handle);
    DestroyStream(&WriteStream);
}

void
FreePackageList(array<package>* PackageList)
{
    for (u64 PackageIndex = 0; PackageIndex < PackageList->Size; ++PackageIndex)
    {
        package* Package = GetElement(PackageList, PackageIndex);

        FreeString(&Package->Name);
        FreeString(&Package->Version);
    }
}

void
UpdateInit(commandline_parser* Parser)
{
    UNUSED_VARIABLE(Parser);
}

void
UpdateRun(json* ConfigFileObject, commandline_parser* Parser)
{
    UNUSED_VARIABLE(Parser);

    if (ConfigFileObject == NULL)
    {
        StandardPrint(CreateString(
            "Failed to find default configuration and none passed through -c or --config option\n"));

        return;
    }

    array<json> SourceObjects;

    GetJsonObjectArray(&SourceObjects, ConfigFileObject, "sources");

    if (SourceObjects.Size == 0)
    {
        StandardPrint(CreateString("Could not read sources from configuration\n"));

        return;
    }

    string UserHomeDirectory;
    string StoragePath;

    GetUserHomeDirectory(&UserHomeDirectory);
    FormatString(&StoragePath, "%str/.local/limelight/", UserHomeDirectory);

    if (!DoesFileExist(StoragePath) && !MakeDirectory(StoragePath))
    {
        string Message;

        FormatString(&Message, "Failed to create limelight directory at: %str\n", StoragePath);
        StandardPrint(Message);
        FreeString(&Message);

        return;
    }

    stream* Stream = (stream*)AllocateMemory(sizeof(stream));
    CURL*   Curl   = InitCurl(Stream);

    for (u64 SourceObjectIndex = 0; SourceObjectIndex < SourceObjects.Size; ++SourceObjectIndex)
    {
        json* SourceObject = SourceObjects + SourceObjectIndex;

        UpdateSource(SourceObject, Curl, Stream, StoragePath);
    }

    FreeString(&StoragePath);
    DestroyCurl(Curl, Stream);
    FreeMemory((char*)Stream, sizeof(stream));
}

void
UpdateSource(json* SourceObject, CURL* Curl, stream* Stream, string StoragePath)
{
    string Name = CreateEmptyString(), Url = CreateEmptyString(), Repos = CreateEmptyString();

    GetJsonString(&Name, SourceObject, "name");
    GetJsonString(&Url, SourceObject, "url");
    GetJsonString(&Repos, SourceObject, "repos");

    if (Url.Length == 0 || Repos.Length == 0 || Name.Length == 0)
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "Must have two json strings url and repos, invalid source object: %str\n",
                     SourceObject->Name);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        return;
    }

    split_string RepoSplit = SplitString(Repos, ' ');

    if (RepoSplit.Count == 0)
    {
        RepoSplit.Buffer = &Repos;
        RepoSplit.Count  = 1;
    }

    for (u64 UrlIndex = 0; UrlIndex < RepoSplit.Count; ++UrlIndex)
    {
        {
            string Message;

            FormatString(&Message, "Updating: %str %str\n", Url, RepoSplit.Buffer[UrlIndex]);
            StandardPrint(Message);
            FreeString(&Message);
        }

        string FullUrl;

        if (EndsWith(Url, CreateString("/")))
        {
            FormatString(&FullUrl, "%str%str/pkg.list", Url, RepoSplit.Buffer[UrlIndex]);
        }
        else
        {
            FormatString(&FullUrl, "%str/%str/pkg.list", Url, RepoSplit.Buffer[UrlIndex]);
        }

        if (!GetUrlRequest(Curl, Stream, FullUrl))
        {
            string ErrorMessage;

            FormatString(&ErrorMessage, "FAILED: could not connect: %str\n", FullUrl);
            StandardPrint(ErrorMessage);
            FreeString(&ErrorMessage);

            FreeString(&FullUrl);
            continue;
        }

        array<package> NewPackageList;

        FreeString(&FullUrl);
        CreateArray(&NewPackageList);
        ParsePackageList(&NewPackageList, Stream->Buffer, Stream->WritePointer);

        for (u64 NewPackageIndex = 0; NewPackageIndex < NewPackageList.Size; ++NewPackageIndex)
        {
            package* Package = NewPackageList + NewPackageIndex;
            string   Message;

            FormatString(&Message, "Package: %str-%str\n", Package->Name, Package->Version);
            StandardPrint(Message);
            FreeString(&Message);
        }

        string PackageListFileName;

        FormatString(&PackageListFileName, "%str%str-%str-pkg.list", StoragePath, Name,
                     RepoSplit.Buffer[UrlIndex]);

        if (DoesFileExist(PackageListFileName))
        {
            file           CurrentPackageListFile   = OpenFile(PackageListFileName, FILE_READ, false);
            char*          CurrentPackageListBuffer = ReadFile(CurrentPackageListFile);
            u64            CurrentPackageListLength = GetFileLength(PackageListFileName);
            array<package> CurrentPackageList;

            CreateArray(&CurrentPackageList);
            ParsePackageList(&CurrentPackageList, CurrentPackageListBuffer, CurrentPackageListLength);
            FreeMemory(CurrentPackageListBuffer, CurrentPackageListLength);
            CloseSystemHandle(&CurrentPackageListFile.Handle);
            DeleteFile(PackageListFileName);

            for (u64 CurrentPackageIndex = 0; CurrentPackageIndex < CurrentPackageList.Size;
                 ++CurrentPackageIndex)
            {
                package* CurrentPackage = CurrentPackageList + CurrentPackageIndex;
                b32      Found          = false;

                for (u64 NewPackageIndex = 0; NewPackageIndex < NewPackageList.Size; ++NewPackageIndex)
                {
                    package* NewPackage = NewPackageList + NewPackageIndex;

                    if (CurrentPackage->Name == NewPackage->Name &&
                        CurrentPackage->Version == CurrentPackage->Version)
                    {
                        Found = true;
                        break;
                    }
                }

                if (!Found)
                {
                    Add(&NewPackageList, *CurrentPackage);
                }
            }
            FreePackageList(&CurrentPackageList);
            FreeArray(&CurrentPackageList);
        }

        WritePackageList(PackageListFileName, &NewPackageList);
        FreeString(&PackageListFileName);
        FreePackageList(&NewPackageList);
        FreeArray(&NewPackageList);
    }
}
