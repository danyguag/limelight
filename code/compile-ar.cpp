#include "compile-ar.h"
#include "platform.h"

#include <dlib1/base.h>

void
CreateStaticLibraryAr(string LibraryName, array<string>* LinkerFlags, array<string>* ObjectFileNames)
{
    array<string> Flags;
    string        Command = CreateString("/usr/bin/ar");

    CreateArray(&Flags);
    Add(&Flags, CreateString("rcs"));
    Add(&Flags, LibraryName);
    Add(&Flags, CreateString("--target=elf64-x86-64"));

    if (LinkerFlags != NULL)
    {
        Add(&Flags, LinkerFlags);
    }

    Add(&Flags, ObjectFileNames);
    CallExecutable(Command, &Flags);

    for (u64 FlagIndex = 0; FlagIndex < Flags.Size; ++FlagIndex)
    {
        FreeString(Flags + FlagIndex);
    }

    FreeArray(&Flags);
}
