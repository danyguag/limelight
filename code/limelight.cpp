#include "limelight.h"
#include "build-file.h"
#include "build-system.h"
#include "pkg-install.h"
#include "pkg-publish.h"
#include "pkg.h"
#include "platform.h"

#include <dlib1/base.h>
#include <dlib1/commandline_parser.h>
#include <dlib1/string.h>
#include <dlib1/timer.h>
#include <dlib1/types.h>

static b32 CreateDefaultConfigName(string* FileName);
static b32 LoadConfigurationFile(json* ConfigFileObject, commandline_parser* Parser);

int
main(int ArgCount, char** Args)
{
    commandline_parser Parser;
    string             BasicUsageInfo = CreateString(
        "This is the Limelight Package System.  Please note the flags shown below include subcommand specific flags that do not apply or necessarily even exist for other subcommands.\n\nBasic Usage:\nlimelight subcommand (other flags)\n\nSubcommands:\nbuild\nupdate\npublish\ninstall\n");

    // Ensure that there is a subcommand
    if (ArgCount < 2)
    {
        StandardPrint(BasicUsageInfo);

        return (0);
    }

    // Setup command line argument information
    CreateCommandlineParser(&Parser, BasicUsageInfo);
    AddFlag(&Parser, CreateString("v"), CreateString("version"), ARGUMENT_TYPE_FLAG,
            CreateString("Gets the current version of limelight"));
    AddFlag(&Parser, CreateString("c"), CreateString("config"), ARGUMENT_TYPE_WORD,
            CreateString("Specifies the path to a configuration file, relative or absolute"));

    char*           SubCommandArg    = Args[1];
    string          SubCommandString = CreateString(SubCommandArg, StringLength(SubCommandArg));
    subcommand_type SubcommandType   = SUBCOMMAND_TYPE_NONE;
    json            ConfigObject;

    engine_memset((char*)&ConfigObject, 0, sizeof(json));

    if (SubCommandString == "build")
    {
        SubcommandType = SUBCOMMAND_TYPE_BUILD;
        BuildInit(&Parser);
    }
    else if (SubCommandString == "update")
    {
        SubcommandType = SUBCOMMAND_TYPE_UPDATE;
        UpdateInit(&Parser);
    }
    else if (SubCommandString == "publish")
    {
        SubcommandType = SUBCOMMAND_TYPE_PUBLISH;
        PublishInit(&Parser);
    }
    else if (SubCommandString == "install")
    {
        SubcommandType = SUBCOMMAND_TYPE_INSTALL;
        InstallInit(&Parser);
    }

    if (SubcommandType != SUBCOMMAND_TYPE_NONE)
    {
        --ArgCount;
        Args = Args + 1;
    }

    // Parse the commandline args
    ParseCommandlineArgs(&Parser, ArgCount, Args);

    if (GetFlagByShortName(&Parser, "v")->Found)
    {
        string Message;

        FormatString(&Message, "limelight: %str\n", VERSION_STRING);
        StandardPrint(Message);
        FreeString(&Message);

        return (0);
    }

    b32 ConfigLoaded = LoadConfigurationFile(&ConfigObject, &Parser);

    if (SubcommandType == SUBCOMMAND_TYPE_NONE)
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "Unknown subcommand type: %str\n", SubCommandString);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        return 0;
    }

    ConfigObject.Name   = CreateEmptyString();
    json* pConfigObject = &ConfigObject;

    if (!ConfigLoaded)
    {
        pConfigObject = NULL;
    }

    if (SubcommandType == SUBCOMMAND_TYPE_BUILD)
    {
        BuildRun(&Parser);
    }
    else if (SubcommandType == SUBCOMMAND_TYPE_UPDATE)
    {
        UpdateRun(pConfigObject, &Parser);
    }
    else if (SubcommandType == SUBCOMMAND_TYPE_PUBLISH)
    {
        PublishRun(pConfigObject, &Parser);
    }
    else if (SubcommandType == SUBCOMMAND_TYPE_INSTALL)
    {
        InstallRun(pConfigObject, &Parser);
    }

    if (ConfigLoaded)
    {
        DestroyJson(&ConfigObject);
    }

    DestroyCommandlineParser(&Parser);

    return (0);
}

static b32
CreateDefaultConfigName(string* FileName)
{
    string UserHomeDirectory;
    string DefaultConfigFileName;

    GetUserHomeDirectory(&UserHomeDirectory);
    FormatString(&DefaultConfigFileName, "%str/.limelight", UserHomeDirectory);

    if (!DoesFileExist(DefaultConfigFileName))
    {
        FreeString(&DefaultConfigFileName);

        return (false);
    }

    *FileName = DefaultConfigFileName;

    return (true);
}

static b32
LoadConfigurationFile(json* ConfigFileObject, commandline_parser* Parser)
{
    flag*  ConfigFileFlag = GetFlagByShortName(Parser, "c");
    string ConfigFileName;

    if (ConfigFileFlag->Found)
    {
        ConfigFileName = ConfigFileFlag->Value;
    }
    else if (!CreateDefaultConfigName(&ConfigFileName))
    {
        return (false);
    }

    file ConfigFile = OpenFile(ConfigFileName, FILE_READ, false);

    if (ConfigFile.Handle < 0)
    {
        if (!ConfigFileFlag->Found)
        {
            FreeString(&ConfigFileName);
        }

        return (false);
    }

    char* Buf       = ReadFile(ConfigFile);
    u64   BufLength = GetFileLength(ConfigFileName);

    if (!ConfigFileFlag->Found)
    {
        FreeString(&ConfigFileName);
    }

    CloseSystemHandle(&ConfigFile.Handle);
    BufferToJSON(ConfigFileObject, Buf, BufLength);
    FreeMemory(Buf, BufLength);

    return (true);
}
