#include "platform.h"

#include <dlib1/base.h>
#include <dlib1/string.h>

#include <errno.h>
#include <pthread.h>
#include <pwd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

struct command_information
{
    string         CommandPath;
    array<string>* Flags;
};

void*
CallExecutableAsync(void* Memory)
{ // Thread for calling executables async
    command_information* CommandInformation = (command_information*)Memory;

    CallExecutable(CommandInformation->CommandPath, CommandInformation->Flags);

    return (NULL);
}

void
CallExecutablesAsync(string CompilerPath, array<array<string>>* CommandFlags, bool wait)
{ // Calls a single executable many times on different threads
    u32                  FlagCount = CommandFlags->Size;
    thread_handle*       Threads   = (thread_handle*)AllocateMemory(sizeof(thread_handle) * FlagCount);
    command_information* Commands =
        (command_information*)AllocateMemory(sizeof(command_information) * FlagCount);

    for (u32 FlagIndex = 0; FlagIndex < FlagCount; ++FlagIndex)
    {
        command_information* CurrentInformation = Commands + FlagIndex;
        CurrentInformation->CommandPath         = CompilerPath;
        CurrentInformation->Flags               = GetElement(CommandFlags, FlagIndex);

        Threads[FlagIndex] = StartThread(&CallExecutableAsync, CurrentInformation);
    }

    if (wait)
    {
        for (u32 ThreadIndex = 0; ThreadIndex < FlagCount; ++ThreadIndex)
        {
            JoinThread(Threads[ThreadIndex]);
            FreeMemory((char*)Threads[ThreadIndex], sizeof(thread_handle));
        }
    }

    FreeMemory((char*)Threads, sizeof(thread_handle) * FlagCount);
    FreeMemory((char*)Commands, sizeof(thread_handle) * FlagCount);
}

b32
CallExecutable(string Path, array<string>* Args)
{ // Calls another executable using execv() syscall
    fflush(stdout);
    int Pid = fork();

    if (Pid == 0)
    {
        u32    ArgCount  = Args->Size;
        char** ArgBuffer = (char**)malloc(sizeof(char*) * (ArgCount + 2));

        ArgBuffer[0] = Path.Buffer;

        for (u32 ArgIndex = 1; ArgIndex < ArgCount + 1; ++ArgIndex)
        {
            ArgBuffer[ArgIndex] = GetElement(Args, ArgIndex - 1)->Buffer;
        }

        ArgBuffer[ArgCount + 1] = NULL;

        if (execv(Path, ArgBuffer) == -1)
        {
            string Out = CreateString(strerror(errno));
            StandardPrint(Out);
            StandardPrint(CreateString("\n"));

            exit(0);
        }
    }
    else
    {
        int ReturnCode;
        waitpid(Pid, &ReturnCode, 0);
    }

    return (true);
}

void
PrintTimer_(string Name, s64 StartTime, s64 EndTime)
{ // Utility function for printing a timed event
    f64    ElapsedTime = ((f64)EndTime - StartTime) / 1000000.0;
    string Message;

    FormatString(&Message, "%str%f64\n", Name, ElapsedTime);
    StandardPrint(Message);
    FreeString(&Message);
}

b32
CopyFile(string& Source, string& Destination)
{ // Copy file from source to destination
    if (DoesFileExist(Destination))
    {
        DeleteFile(Destination);
    }

    file SourceFile      = OpenFile(Source, FILE_READ, false);
    file DestinationFile = OpenFile(Destination, FILE_WRITE, true);

    if (SourceFile.Handle < 0 || DestinationFile.Handle < 0)
    {
        return (false);
    }

    char* SourceContents = ReadFile(SourceFile);
    u64 SourceContentLength = GetFileLength(Source);

    WriteFile(DestinationFile, SourceContents, SourceContentLength);

    FreeMemory(SourceContents, SourceContentLength);
    CloseSystemHandle(&SourceFile.Handle);
    CloseSystemHandle(&DestinationFile.Handle);

    return (true);
}

b32
SetExecutable(string& DestinationFile)
{
    return (chmod(DestinationFile, S_IRWXU | S_IXGRP | S_IXOTH) == 0);
}

void
GetUserHomeDirectory(string* Out)
{
    struct passwd* pw = getpwuid(getuid());

    *Out = CreateString(pw->pw_dir);
}
