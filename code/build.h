#if !defined(BUILD_H)

#include "dependencies.h"

#include <dlib1/array.h>
#include <dlib1/commandline_parser.h>
#include <dlib1/json.h>

struct build;

#define DESTROY_COMPILATION_USERDATA(name) void name(void* UserData)
typedef DESTROY_COMPILATION_USERDATA(destroy_compilation_userdata);

#define CREATE_COMPILATION_OBJECTS(name) b32 name(build* Build, void* UserData)
typedef CREATE_COMPILATION_OBJECTS(create_compilation_objects);

#define CREATE_LIBRARY_STATIC(name) b32 name(build* Build, void* UserData)
typedef CREATE_LIBRARY_STATIC(create_library_static);

#define CREATE_LIBRARY_SHARED(name) b32 name(build* Build, void* UserData)
typedef CREATE_LIBRARY_SHARED(create_library_shared);

#define CREATE_EXECUTABLE(name) b32 name(build* Build, void* UserData)
typedef CREATE_EXECUTABLE(create_executable);

enum build_options
{
    BUILD_OPTION_DEBUG                     = 1 << 0,
    BUILD_OPTION_RELEASE                   = 1 << 1,
    BUILD_OPTION_GENERATE_COMPILE_COMMANDS = 1 << 2,
    BUILD_OPTION_SHARED                    = 1 << 3,
    BUILD_OPTION_STATIC                    = 1 << 4,
    BUILD_OPTION_INSTALL                   = 1 << 5,
};

enum build_type
{
    BUILD_TYPE_NONE,
    BUILD_TYPE_DEBUG,
    BUILD_TYPE_RELEASE,
};

enum output_type
{
    OUTPUT_TYPE_NONE,
    OUTPUT_TYPE_EXECUTABLE,
    OUTPUT_TYPE_SHARED,
    OUTPUT_TYPE_STATIC,
};

enum build_hook_type
{
    BUILD_HOOK_TYPE_NONE    = 0,
    BUILD_HOOK_TYPE_COMMAND = 1,
    BUILD_HOOK_TYPE_BUILD   = 2
};

enum build_hook_execution_time
{
    BUILD_HOOK_EXECUTION_TIME_NONE = 0,
    BUILD_HOOK_EXECUTION_TIME_PRE  = 1,
    BUILD_HOOK_EXECUTION_TIME_POST = 2
};

struct comp_file
{
    string FullName;
    string Path;
    string Name;
    string ObjectFileName;
    string Command;
};

struct build_hook
{
    build_hook_type           Type;
    build_hook_execution_time RunTime;
    string                    Path;
};

struct build
{
    json* BuildObject;

    build_type  BuildType;  //
    output_type OutputType; //
    b32         Result;

    destroy_compilation_userdata* DestroyCompilationUserData;
    create_library_static*        CreateLibraryStatic;
    create_library_shared*        CreateLibraryShared;
    create_executable*            CreateExecutable;
    void*                         UserData;

    array<string> IncludeFolders; //
    array<string> LibraryPaths;   //

    string           BaseFolder;
    string           Name;            //
    string           OutputDirectory; //
    string           ExecutableName;  //
    array<comp_file> InputFiles;      //
    comp_file        InputFile;       //

    array<build_hook> BuildHooks;

    string InstallationDirectory;
    string HeaderInstallationDirectory;
    string IncludeDirectory;

    // This is anything that a specific application may need, I may want to make this a little more
    // robust because right now it is only used for linking libraries with pkg-config
    // and with systen libraries, so i could add one for general flags to like -Wno-'error-name'
    // so that these are not baked in automatically
    // I also want to add something so that i can add preprocessor defines
    array<string> ExtraFlags; //

    dependencies Dependencies;
};

void DestroyBuilds(array<build>* Builds);
void ExecuteBuilds(array<build>* Builds, u32 OptionBits);

void RunBuildHooks(build* Build);
void CreateCompileCommandFile(build* Build);
void GenerateCompileCommand(string* Command, string& CompilerPath, array<string>* CurrentObjectFileArgs);

#define BUILD_H
#endif
