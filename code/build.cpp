#include "build.h"
#include "build-file.h"
#include "compile-clang.h"
#include "compile-gpp.h"
#include "platform.h"

#include <dlib1/base.h>
#include <dlib1/commandline_parser.h>
#include <dlib1/stream.h>
#include <dlib1/string.h>
#include <dlib1/timer.h>
#include <dlib1/types.h>

void ExecuteOrLinkBuilds(build* Build, array<build>* Builds);
b32  FindInFileBuildDepedencies(array<build>* Builds, string Name);
void RunBuildTypeBuilds(array<build>* Builds, build* Build, build_hook* BuildHook);
void RunBuildHooks(array<build>* Builds, build* Build, build_hook_execution_time CurrentTime);
b32  CheckBuild(build* Build);
b32  SetupBuild(build* Build);
b32  CompileBuild(build* Build);
void CleanBuild(build* Build);
b32  InstallBuild(build* Build);

void
ExecuteBuilds(array<build>* Builds, u32 OptionsBits)
{
    build* Build;
    s64    StartRunTime;

    for (u64 BuildIndex = 0; BuildIndex < Builds->Size; ++BuildIndex)
    {
        Build         = GetElement(Builds, BuildIndex);
        StartRunTime  = GetTime();
        Build->Result = false;

        {
            string Message;

            FormatString(&Message, "Building %str.\n", Build->BuildObject->Name);
            StandardPrint(Message);
            FreeString(&Message);
        }

        CreateArray(&Build->InputFiles);

        // Converts the build json object into all of the correct fields in the build object
        if (!SetupBuild(Build))
        {
            PrintTimer(CreateString("Build Failed "), StartRunTime);

            break;
        }

        if (OptionsBits & BUILD_OPTION_DEBUG)
        {
            Build->BuildType = BUILD_TYPE_DEBUG;
        }
        else if (OptionsBits & BUILD_OPTION_RELEASE)
        {
            Build->BuildType = BUILD_TYPE_RELEASE;
        }

        if (Build->OutputType != OUTPUT_TYPE_EXECUTABLE && OptionsBits & BUILD_OPTION_SHARED)
        {
            Build->OutputType = OUTPUT_TYPE_SHARED;
        }
        else if (Build->OutputType != OUTPUT_TYPE_EXECUTABLE && OptionsBits & BUILD_OPTION_RELEASE)
        {
            Build->OutputType = OUTPUT_TYPE_STATIC;
        }

        // Deletes all existing build files from the build directory
        CleanBuild(Build);

        // Checks to make sure the build object was initialized correctly
        CheckBuild(Build);

        // Builds the information that is needed to link to dependencies
        BuildDependencies(&Build->Dependencies, &Build->IncludeFolders, &Build->LibraryPaths);

        // Run all of the build hooks that are supposed to run before the build
        RunBuildHooks(Builds, Build, BUILD_HOOK_EXECUTION_TIME_PRE);

        string BuildDirectory;

        FormatString(&BuildDirectory, "%str/%str", Build->BaseFolder, Build->OutputDirectory);
        SetWorkingDirectory(BuildDirectory);

        // Invokes compiler
        if (!CompileBuild(Build))
        {
            PrintTimer(CreateString("Build Failed "), StartRunTime);

            break;
        }

        // Installs the build to the specified location if requested
        if (OptionsBits & BUILD_OPTION_INSTALL)
        {
            if (!InstallBuild(Build))
            { // Return a failure if the installation failed
                string ErrorMessage;

                FormatString(&ErrorMessage, "Failed to install: %str\n", Build->BuildObject->Name);
                StandardPrint(ErrorMessage);
                FreeString(&ErrorMessage);
            }
        }

        // Brute force way of creating compile commands json file if requested
        if (OptionsBits & BUILD_OPTION_GENERATE_COMPILE_COMMANDS)
        {
            CreateCompileCommandFile(Build);
        }

        Build->Result = true;
        PrintTimer(CreateString("Build Successful "), StartRunTime);

        // Run all the build hooks only after a successful build
        RunBuildHooks(Builds, Build, BUILD_HOOK_EXECUTION_TIME_POST);
    }
}

void
RunBuildHooks(array<build>* Builds, build* Build, build_hook_execution_time CurrentTime)
{
    array<build_hook> BuildHooks     = Build->BuildHooks;
    u64               BuildHookCount = BuildHooks.Size;

    if (BuildHookCount > 0)
    {
        string Message;

        if (BuildHookCount > 1)
        {
            FormatString(&Message, "Running Build Hooks: %str\n", Build->Name);
        }
        else
        {
            FormatString(&Message, "Running Build Hook: %str\n", Build->Name);
        }

        StandardPrint(Message);
        FreeString(&Message);

        for (u64 BuildHookIndex = 0; BuildHookIndex < BuildHookCount; ++BuildHookIndex)
        {
            build_hook* BuildHook = BuildHooks + BuildHookIndex;

            if (BuildHook->RunTime != CurrentTime)
            {
                continue;
            }

            s64 BuildHookStartTime = GetTime();

            // Determines if the build hook is another build directory or an executable
            if (BuildHook->Type == BUILD_HOOK_TYPE_BUILD)
            {
                RunBuildTypeBuilds(Builds, Build, BuildHook);
            }
            else if (BuildHook->Type == BUILD_HOOK_TYPE_COMMAND)
            { // The build hook is just an executable
                array<string> Flags;
                split_string  CommandSplit = SplitString(BuildHook->Path, ' ');
                string        Command;

                CreateArray(&Flags);

                if (CommandSplit.Count > 0)
                {
                    Command = CommandSplit.Buffer[0];

                    for (u32 ArgIndex = 1; ArgIndex < CommandSplit.Count; ++ArgIndex)
                    {
                        string CurrentArg = CommandSplit.Buffer[ArgIndex];

                        Add(&Flags, CurrentArg);

                        FreeString(&CurrentArg);
                    }
                }
                else
                {
                    Command = BuildHook->Path;
                }

                CallExecutable(Command, &Flags);

                for (u64 FlagIndex = 0; FlagIndex < Flags.Size; ++FlagIndex)
                {
                    FreeString(Flags + FlagIndex);
                }

                FreeArray(&Flags);
            }

            PrintTimer(CreateString("Build Hooke Completed "), BuildHookStartTime);
        }
    }
}

void
RunBuildTypeBuilds(array<build>* Builds, build* Build, build_hook* BuildHook)
{
    string     BuildHookDirectory;
    u64        PathEndIndex = BuildHook->Path.Length - 1;
    string     BuildFileName;
    build_file BuildFile;

    engine_memset((char*)&BuildFile, 0, sizeof(build_file));

    if (BuildHook->RunTime == BUILD_HOOK_EXECUTION_TIME_POST &&
        (Build->OutputType == OUTPUT_TYPE_SHARED || Build->OutputType == OUTPUT_TYPE_STATIC))
    {
        string ErrorMessage;
        string OutputTypeString = CreateString("Static Library");

        if (Build->OutputType == OUTPUT_TYPE_SHARED)
        {
            OutputTypeString = CreateString("Shared Library");
        }

        FormatString(&ErrorMessage, "Post Build Hook Unsupported Build Type: %str\n", OutputTypeString);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        return;
    }

    b32 IsOtherBuild = FindInFileBuildDepedencies(Builds, BuildHook->Path);

    if (IsOtherBuild)
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "Post Build Hook Unsupported Build Type Dependent Build: %str\n",
                     BuildHook->Path);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        return;
    }

    while (BuildHook->Path.Buffer[PathEndIndex] != '/')
    {
        if (PathEndIndex == 0)
        {
            break;
        }

        --PathEndIndex;
    }

    if (PathEndIndex > 0)
    {
        SubString(&BuildHookDirectory, BuildHook->Path, 0, PathEndIndex + 1);
        SubString(&BuildFileName, BuildHook->Path, PathEndIndex + 1, BuildHook->Path.Length);

        // Set the correct working directory of the build hook and being the build process
        if (!SetWorkingDirectory(BuildHookDirectory))
        {
            string Message;

            FormatString(&Message, "Could not change to build hook build directory: %str\n", BuildHook->Path);
            StandardPrint(Message);
            FreeString(&BuildHookDirectory);

            return;
        }
    }
    else
    {
        CopyString(&BuildFileName, BuildHook->Path);
    }

    FreeString(&BuildHookDirectory);

    if (ParseBuildFile(&BuildFile, BuildFileName))
    {
        u32 OptionsBits = 0;

        if (Build->BuildType == BUILD_TYPE_DEBUG)
        {
            OptionsBits |= BUILD_OPTION_DEBUG;
        }

        if (Build->BuildType == BUILD_TYPE_RELEASE)
        {
            OptionsBits |= BUILD_OPTION_RELEASE;
        }

        ExecuteBuilds(&BuildFile.Builds, OptionsBits);

        ExecuteOrLinkBuilds(Build, &BuildFile.Builds);

        DestroyBuildFile(&BuildFile);
    }

    FreeString(&BuildFileName);
}

void
ExecuteOrLinkBuilds(build* ParentBuild, array<build>* Builds)
{
    for (u64 BuildIndex = 0; BuildIndex < Builds->Size; ++BuildIndex)
    {
        build* Build = GetElement(Builds, BuildIndex);

        if (!Build->Result)
        {
            continue;
        }

        // Run the newly generated
        if (Build->OutputType == OUTPUT_TYPE_EXECUTABLE)
        {
            string ExecutablePath;

            FormatString(&ExecutablePath, "%str/%str\n", Build->OutputDirectory, Build->ExecutableName);
            StandardPrint(ExecutablePath);

            continue;
        }

        string* NewLibraryString = Add(&ParentBuild->Dependencies.LibraryStrings);

        if (Build->OutputType == OUTPUT_TYPE_SHARED)
        {
            FormatString(NewLibraryString, "%str/%str/lib%str.so", Build->BaseFolder, Build->OutputDirectory,
                         Build->ExecutableName);
        }
        else if (Build->OutputType == OUTPUT_TYPE_STATIC)
        {
            FormatString(NewLibraryString, "%str/%str/lib%str.a", Build->BaseFolder, Build->OutputDirectory,
                         Build->ExecutableName);
        }
    }
}

b32
FindInFileBuildDepedencies(array<build>* Builds, string Name)
{
    for (u64 BuildIndex = 0; BuildIndex < Builds->Size; ++BuildIndex)
    {
        build* Build = GetElement(Builds, BuildIndex);

        if (Build->Name == Name)
        {
            return (true);
        }
    }

    return (false);
}

void
CreateCompileCommandFile(build* Build)
{
    UNUSED_VARIABLE(Build);
    /*    stream WriteStream;
        CreateStream(&WriteStream, Megabytes(100));

        u64 CommandCount = Build->InputFiles.Size;

        if (Build

        Write<char>(&WriteStream, '[');

        string CommandStart              = CreateString("{\"directory\":\"");
        string CommandDirectoryToCommand = CreateString("\",\"command\":\"");
        string CommandCommandToFile      = CreateString("\",\"file\":\"");
        string CommandFileToEnd          = CreateString("\"},");

        for (u32 CommandIndex = 0; CommandIndex < CommandCount; ++CommandIndex)
        {
            comp_file* CurrentFile = Build->CompileCommands + CommandIndex;
            Write(&WriteStream, CommandStart.Buffer, CommandStart.Length);
            Write(&WriteStream, CurrentFile->Path.Buffer, CurrentFile->Path.Length);
            Write(&WriteStream, CommandDirectoryToCommand.Buffer, CommandDirectoryToCommand.Length);
            Write(&WriteStream, CurrentFile->Command.Buffer, CurrentFile->Command.Length);
            Write(&WriteStream, CommandCommandToFile.Buffer, CommandCommandToFile.Length);
            Write(&WriteStream, CurrentFile->FullName.Buffer, CurrentFile->FullName.Length);
            Write(&WriteStream, CommandFileToEnd.Buffer, CommandFileToEnd.Length);
        }

        Write<char>(&WriteStream, ']');

        string CurrentWorkingDirectory;
        GetCurrentWorkingDirectory(&CurrentWorkingDirectory);

        // Make sure the user is warned when the expected folder for the compile commands can not be accessed
        if (!SetWorkingDirectory(Build->BaseFolder))
        {
            string ErrorMessage;

            FormatString(&ErrorMessage,
                         "Failed to switch back to base folder: %str, defaulting to current directory\n",
                         Build->BaseFolder);
            StandardPrint(ErrorMessage);
            FreeString(&ErrorMessage);
        }

        file CompileCommandsFile                     = OpenFile("compile_commands.json", FILE_WRITE, true);
        WriteStream.Buffer[WriteStream.WritePointer] = 0;
        WriteFile(CompileCommandsFile, WriteStream.Buffer, WriteStream.WritePointer);

        CloseSystemHandle(&CompileCommandsFile.Handle);

        SetWorkingDirectory(CurrentWorkingDirectory);*/
}

// Converts file name into a comp_file that the build_system can use
static comp_file
CreateCompilationFile(string BaseFolder, string FullName)
{
    if (!FullName)
    {
        return { CreateEmptyString(), CreateEmptyString(), CreateEmptyString(), CreateEmptyString(),
                 CreateEmptyString() };
    }

    string ObjectFileName = CreateEmptyString();
    string Name           = CreateEmptyString();
    string Path           = CreateEmptyString();
    string FullNameRes    = CreateEmptyString();
    u32    StartOffset    = 0;

    for (u32 CharIndex = 0; CharIndex < FullName.Length - 3; ++CharIndex)
    {
        char Pos1 = FullName.Buffer[CharIndex];
        char Pos2 = FullName.Buffer[CharIndex + 1];
        char Pos3 = FullName.Buffer[CharIndex + 2];
        char Pos4 = FullName.Buffer[CharIndex + 3];

        if (Pos1 == '/')
        {
            StartOffset = CharIndex + 1;
        }

        if (Pos1 == '.' && Pos2 == 'c' && Pos3 == 'p' && Pos4 == 'p')
        {
            SubString(&Name, FullName, StartOffset, CharIndex);

            if (StartOffset > 1)
            {
                SubString(&Path, FullName, 0, StartOffset - 1);
            }

            if (FullName.Buffer[0] != '/')
            {
                if (!Path)
                {
                    CopyString(&Path, BaseFolder);
                    FormatString(&FullNameRes, "%str/%str", Path, FullName);
                }
                else
                {
                    string PreviousPath = Path;

                    FormatString(&Path, "%str/%str", BaseFolder, PreviousPath);
                    FormatString(&FullNameRes, "%str/%str", BaseFolder, FullName);
                    FreeString(&PreviousPath);
                }
            }
            FormatString(&ObjectFileName, "%str.o", Name);

            return { FullNameRes, Path, Name, ObjectFileName, CreateEmptyString() };
        }
    }

    string ErrorMessage;

    FormatString(&ErrorMessage, "Error could not parse file name: %str\n", FullName);
    StandardPrint(ErrorMessage);
    FreeString(&ErrorMessage);
    exit(0);
}

static void
DestroyCompilationFile(comp_file* File)
{
    if (File->FullName.Length > 0)
    {
        FreeString(&File->FullName);
    }
    if (File->Path.Length > 0)
    {
        FreeString(&File->Path);
    }
    if (File->Name.Length > 0)
    {
        FreeString(&File->Name);
    }
    if (File->ObjectFileName.Length > 0)
    {
        FreeString(&File->ObjectFileName);
    }
    if (File->Command.Length > 0)
    {
        FreeString(&File->Command);
    }
}

b32
CheckBuild(build* Build)
{
    if (!Build->Name)
    {
        return (false);
    }

    if (Build->OutputType == OUTPUT_TYPE_NONE)
    {
        return (false);
    }

    if (Build->BuildType == BUILD_TYPE_NONE)
    {
        return (false);
    }

    if (!Build->OutputDirectory)
    {
        return (false);
    }

    if (!Build->InputFile.FullName && Build->InputFiles.Size == 0)
    {
        return (false);
    }

    return (true);
}

b32
SetupBuild(build* Build)
{
    string OutputTypeString;
    GetJsonString(&OutputTypeString, Build->BuildObject, "output_type");

    Build->OutputType = OUTPUT_TYPE_NONE;

    if (OutputTypeString == "executable")
    {
        Build->OutputType = OUTPUT_TYPE_EXECUTABLE;
    }
    if (OutputTypeString == "static")
    {
        Build->OutputType = OUTPUT_TYPE_STATIC;
    }
    if (OutputTypeString == "shared")
    {
        Build->OutputType = OUTPUT_TYPE_SHARED;
    }

    if (Build->OutputType == OUTPUT_TYPE_NONE)
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "Error: Invalid Output type: %str\n", OutputTypeString);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        return (false);
    }

    string BuildTypeString;
    GetJsonString(&BuildTypeString, Build->BuildObject, "build_type");

    Build->BuildType = BUILD_TYPE_NONE;

    if (BuildTypeString == "release")
    {
        Build->BuildType = BUILD_TYPE_RELEASE;
    }
    if (BuildTypeString == "debug")
    {
        Build->BuildType = BUILD_TYPE_DEBUG;
    }

    if (Build->BuildType == BUILD_TYPE_NONE)
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "Error: Invalid Build type: %str\n", BuildTypeString);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        return (false);
    }

    array<string> InputFiles;
    GetJsonStringArray(&InputFiles, Build->BuildObject, "input_files");

    if (InputFiles.AllocatedLength > 0)
    {
        u32 InputFileCount = InputFiles.Size;

        for (u32 InputFileIndex = 0; InputFileIndex < InputFileCount; ++InputFileIndex)
        {
            string CurrentFileName = InputFiles[InputFileIndex];

            comp_file* NewBuildFile = Add(&Build->InputFiles);

            *NewBuildFile = CreateCompilationFile(Build->BaseFolder, CurrentFileName);
        }
    }

    array<json> BuildHooks;
    GetJsonObjectArray(&BuildHooks, Build->BuildObject, "build_hooks");

    if (BuildHooks.AllocatedLength > 0)
    {
        u64 BuildHookCount = BuildHooks.Size;

        CreateArray(&Build->BuildHooks, HEAP_ALLOCATOR_MEMORY_CALLBACK(), BuildHookCount);

        for (u32 BuildHookIndex = 0; BuildHookIndex < BuildHookCount; ++BuildHookIndex)
        {
            json*       BuildHookObject     = BuildHooks + BuildHookIndex;
            build_hook* BuildHook           = Add(&Build->BuildHooks);
            string      HookTypeString      = CreateEmptyString();
            string      ExecutionTimeString = CreateEmptyString();

            GetJsonString(&HookTypeString, BuildHookObject, "type");
            GetJsonString(&ExecutionTimeString, BuildHookObject, "time");
            GetJsonString(&BuildHook->Path, BuildHookObject, "path");

            if (HookTypeString == "command")
            {
                BuildHook->Type = BUILD_HOOK_TYPE_COMMAND;
            }
            else if (HookTypeString == "build")
            {
                BuildHook->Type = BUILD_HOOK_TYPE_BUILD;
            }
            else
            {
                BuildHook->Type = BUILD_HOOK_TYPE_NONE;
            }

            if (ExecutionTimeString == "pre")
            {
                BuildHook->RunTime = BUILD_HOOK_EXECUTION_TIME_PRE;
            }
            else if (ExecutionTimeString == "post")
            {
                BuildHook->RunTime = BUILD_HOOK_EXECUTION_TIME_POST;
            }
            else
            {
                BuildHook->RunTime = BUILD_HOOK_EXECUTION_TIME_NONE;
            }
        }
    }

    Build->Name = Build->BuildObject->Name;
    GetJsonString(&Build->OutputDirectory, Build->BuildObject, "output_directory");

    if (Build->OutputType == OUTPUT_TYPE_EXECUTABLE)
    {
        string InputFileString = CreateEmptyString();
        GetJsonString(&InputFileString, Build->BuildObject, "input_file");
        Build->InputFile = CreateCompilationFile(Build->BaseFolder, InputFileString);
    }

    GetJsonString(&Build->ExecutableName, Build->BuildObject, "executable_name");

    string ExtraFlagString = CreateEmptyString();
    GetJsonString(&ExtraFlagString, Build->BuildObject, "extra_flags");

    if (ExtraFlagString.Length > 0)
    {
        split_string ExtraFlags = SplitString(ExtraFlagString, ' ');

        CreateArray(&Build->ExtraFlags);

        for (u32 SplitIndex = 0; SplitIndex < ExtraFlags.Count; ++SplitIndex)
        {
            string CurrentSplit = ExtraFlags.Buffer[SplitIndex];

            Add(&Build->ExtraFlags, CurrentSplit);

            FreeString(&CurrentSplit);
        }

        FreeMemory((char*)ExtraFlags.Buffer, sizeof(string) * ExtraFlags.Count);
    }

    GetJsonString(&Build->InstallationDirectory, Build->BuildObject, "installation_directory");
    GetJsonString(&Build->HeaderInstallationDirectory, Build->BuildObject, "header_installation_directory");
    GetJsonString(&Build->IncludeDirectory, Build->BuildObject, "include_directory");
    GetJsonStringArray(&Build->IncludeFolders, Build->BuildObject, "includes");
    GetJsonStringArray(&Build->LibraryPaths, Build->BuildObject, "libs");

    string CompilerType;
    GetJsonString(&CompilerType, Build->BuildObject, "compiler_type");

    if (CompilerType == "g++")
    {
        GetGppCompilationProcess(Build);
    }
    else if (CompilerType == "clang")
    {
        GetClangCompilationProcess(Build);
    }

    return (true);
}

void
CleanBuild(build* Build)
{
    if (!SetWorkingDirectory(Build->OutputDirectory))
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "Output Directory: %str: does not exist\n", Build->OutputDirectory);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);
        exit(0);
    }

    string RealExecutableName = Build->ExecutableName;

    if (Build->OutputType == OUTPUT_TYPE_SHARED)
    {
        FormatString(&RealExecutableName, "%str.so", Build->ExecutableName);
    }
    else if (Build->OutputType == OUTPUT_TYPE_STATIC)
    {
        FormatString(&RealExecutableName, "%str.a", Build->ExecutableName);
    }

    // Delete executable if it exists
    if (DoesFileExist(RealExecutableName))
    {
        DeleteFile(RealExecutableName);
    }

    if (Build->OutputType == OUTPUT_TYPE_SHARED || Build->OutputType == OUTPUT_TYPE_STATIC)
    {
        FreeString(&RealExecutableName);
    }

    u32 ObjectFileCount = Build->InputFiles.Size;

    // Delete all object files
    for (u32 ObjectFileIndex = 0; ObjectFileIndex < ObjectFileCount; ++ObjectFileIndex)
    {
        comp_file* File = Build->InputFiles + ObjectFileIndex;

        if (DoesFileExist(File->ObjectFileName))
        {
            DeleteFile(File->ObjectFileName);
        }
    }

    if (Build->OutputType == OUTPUT_TYPE_SHARED)
    {
        FreeString(&RealExecutableName);
    }
}

b32
CompileBuild(build* Build)
{
    if (!SetWorkingDirectory(Build->OutputDirectory))
    {
        string ErrorMessage;

        FormatString(&ErrorMessage, "Output Directory: %str: does not exist", Build->OutputDirectory);
        StandardPrint(ErrorMessage);
        FreeString(&ErrorMessage);

        return (false);
    }

    u64 InputFileCount = Build->InputFiles.Size;

    // Create a static library if the build type is static and there are input files
    if (InputFileCount > 0 && Build->OutputType == OUTPUT_TYPE_STATIC && Build->CreateLibraryStatic != NULL &&
        !Build->CreateLibraryStatic(Build, Build->UserData))
    {
        return (false);
    }

    // Create a shared library if the build type is shared and there are input files
    if (InputFileCount > 0 && Build->OutputType == OUTPUT_TYPE_SHARED && Build->CreateLibraryShared != NULL &&
        !Build->CreateLibraryShared(Build, Build->UserData))
    {
        return (false);
    }

    // Create an executable
    if (Build->OutputType == OUTPUT_TYPE_EXECUTABLE && Build->CreateExecutable != NULL &&
        !Build->CreateExecutable(Build, Build->UserData))
    {
        return (false);
    }

    if (Build->DestroyCompilationUserData != NULL)
    {
        Build->DestroyCompilationUserData(Build->UserData);
    }

    return true;
}

b32
InstallBuild(build* Build)
{
    b32 Result = true;

    // Install the executable if its the build type
    if (Build->OutputType == OUTPUT_TYPE_EXECUTABLE)
    {
        {
            string Message;

            FormatString(&Message, "Installing executable in %str\n", Build->InstallationDirectory);
            StandardPrint(Message);
            FreeString(&Message);
        }

        string SourceFileName;
        string DestinationFileName;

        FormatString(&SourceFileName, "./%str", Build->ExecutableName);

        if (EndsWith(Build->InstallationDirectory, CreateString("/")))
        {
            FormatString(&DestinationFileName, "%str%str", Build->InstallationDirectory,
                         Build->ExecutableName);
        }
        else
        {
            FormatString(&DestinationFileName, "%str/%str", Build->InstallationDirectory,
                         Build->ExecutableName);
        }

        if (DoesFileExist(DestinationFileName))
        {
            DeleteFile(DestinationFileName);
        }

        if (!CopyFile(SourceFileName, DestinationFileName))
        {
            StandardPrint(CreateString("Failed to install executable\n"));
            Result = false;
        }
        else
        {
            StandardPrint(CreateString("Executable installation successful\n"));

            if (!SetExecutable(DestinationFileName))
            {
                StandardPrint(CreateString("Failed to change the executable file to executable\n"));
                Result = false;
            }
        }
    }
    else if (Build->OutputType == OUTPUT_TYPE_SHARED || Build->OutputType == OUTPUT_TYPE_STATIC)
    { // Install the libraries and header files
        {
            string Message;

            FormatString(&Message, "Installing library in %str\nInstalling header files in %str\n",
                         Build->InstallationDirectory, Build->HeaderInstallationDirectory);
            StandardPrint(Message);
            FreeString(&Message);
        }

        string SourceFileName;
        string DestinationFileName;

        FormatString(&SourceFileName, "./%str", Build->ExecutableName);

        if (EndsWith(Build->InstallationDirectory, CreateString("/")))
        {
            FormatString(&DestinationFileName, "%str%str", Build->InstallationDirectory,
                         Build->ExecutableName);
        }
        else
        {
            FormatString(&DestinationFileName, "%str/%str", Build->InstallationDirectory,
                         Build->ExecutableName);
        }

        if (DoesFileExist(DestinationFileName))
        {
            DeleteFile(DestinationFileName);
        }

        if (!CopyFile(SourceFileName, DestinationFileName))
        {
            StandardPrint(CreateString("Failed to install library\n"));
            Result = false;
        }
        else
        {
            StandardPrint(CreateString("Library installation Successful\n"));
        }

        array<io_object> Objects      = SearchFolder(Build->IncludeDirectory);
        u32              ObjectCount  = Objects.Size;
        b32              HeaderResult = true;

        for (u32 ObjectIndex = 0; ObjectIndex < ObjectCount; ++ObjectIndex)
        {
            io_object* Object = Objects + ObjectIndex;

            if (Object->Type == IO_FILE)
            {
                if (EndsWith(Object->Name, CreateString(".h")))
                {
                    string HeaderSource;
                    string HeaderDestination;

                    if (EndsWith(Build->HeaderInstallationDirectory, CreateString("/")))
                    {
                        FormatString(&HeaderDestination, "%str%str", Build->HeaderInstallationDirectory,
                                     Object->Name);
                    }
                    else
                    {
                        FormatString(&HeaderDestination, "%str/%str", Build->HeaderInstallationDirectory,
                                     Object->Name);
                    }

                    if (EndsWith(Build->IncludeDirectory, CreateString("/")))
                    {
                        FormatString(&HeaderSource, "%str%str", Build->IncludeDirectory, Object->Name);
                    }
                    else
                    {
                        FormatString(&HeaderSource, "%str/%str", Build->IncludeDirectory, Object->Name);
                    }

                    if (DoesFileExist(HeaderDestination))
                    {
                        DeleteFile(HeaderDestination);
                    }

                    if (!CopyFile(HeaderSource, HeaderDestination))
                    {
                        string ErrorMessage;

                        FormatString(&ErrorMessage, "Failed to install header file %str at %str\n",
                                     Object->Name, HeaderDestination);
                        StandardPrint(ErrorMessage);
                        FreeString(&ErrorMessage);

                        Result       = false;
                        HeaderResult = false;
                    }

                    FreeString(&HeaderDestination);
                }
            }
        }

        if (HeaderResult)
        {
            StandardPrint(CreateString("Header installation successful\n"));
        }
        else
        {
            StandardPrint(CreateString("Failed to install header files\n"));
        }
    }

    return (Result);
}

void
DestroyBuilds(array<build>* Builds)
{
    for (u64 BuildIndex = 0; BuildIndex < Builds->Size; ++BuildIndex)
    {
        build* Build = GetElement(Builds, BuildIndex);

        if (Build->InputFiles.AllocatedLength > 0)
        {
            for (u64 InputFileIndex = 0; InputFileIndex < Build->InputFiles.Size; ++InputFileIndex)
            {
                comp_file* File = Build->InputFiles + InputFileIndex;

                DestroyCompilationFile(File);
            }

            FreeArray(&Build->InputFiles);
        }

        DestroyCompilationFile(&Build->InputFile);
        FreeArray(&Build->BuildHooks);

        if (Build->ExtraFlags.AllocatedLength > 0)
        {
            for (u64 ExtraFlagIndex = 0; ExtraFlagIndex < Build->ExtraFlags.Size; ++ExtraFlagIndex)
            {
                FreeString(Build->ExtraFlags + ExtraFlagIndex);
            }

            FreeArray(&Build->ExtraFlags);
        }

        if (Build->IncludeFolders.Size > 0)
        {
            for (u64 IncludeIndex = 0; IncludeIndex < Build->Dependencies.IncludeStrings.Size; ++IncludeIndex)
            {
                FreeString(Build->Dependencies.IncludeStrings + IncludeIndex);
            }

            FreeArray(&Build->Dependencies.IncludeStrings);
        }

        if (Build->LibraryPaths.Size > 0)
        {
            for (u64 LibraryIndex = 0; LibraryIndex < Build->Dependencies.LibraryStrings.Size; ++LibraryIndex)
            {
                FreeString(Build->Dependencies.LibraryStrings + LibraryIndex);
            }

            FreeArray(&Build->Dependencies.LibraryStrings);
        }
    }
}

void
GenerateCompileCommand(string* Command, string& CompilerPath, array<string>* Flags)
{
    stream WriteStream;
    CreateStream(&WriteStream);

    Write(&WriteStream, CompilerPath.Buffer, CompilerPath.Length);
    Write(&WriteStream, ' ');

    u32 FlagCount = Flags->Size;

    for (u32 FlagIndex = 0; FlagIndex < FlagCount; ++FlagIndex)
    {
        string* Current = GetElement(Flags, FlagIndex);

        Write(&WriteStream, Current->Buffer, Current->Length);

        if (FlagIndex + 1 < FlagCount)
        {
            Write(&WriteStream, ' ');
        }
    }

    Command->Buffer   = WriteStream.Buffer;
    Command->Length   = WriteStream.WritePointer;
    Command->Capacity = WriteStream.Length;
}
