#include "build-file.h"

#include <dlib1/base.h>
#include <dlib1/json.h>
#include <dlib1/memory.h>
#include <dlib1/stream.h>
#include <dlib1/string.h>
#include <dlib1/types.h>

#include "platform.h"

static b32
ReadRealBuildFile(json* BuildFileObject, string BuildFileName, string InitialDirectory)
{
    file   BuildFile;
    string FullBuildFileName;
    char*  Buf;
    u64    BufLength; // TODO: Remove when ReadFile gets fixed

    FormatString(&FullBuildFileName, "%str/%str", InitialDirectory, BuildFileName);
    BuildFile = OpenFile(FullBuildFileName, FILE_READ, false);

    if (BuildFile.Handle < 0)
    {
        FreeString(&FullBuildFileName);
        return (false);
    }

    Buf       = ReadFile(BuildFile);
    BufLength = GetFileLength(FullBuildFileName);

    string ActualBuildDirectory = CreateEmptyString();

    BufferToJSON(BuildFileObject, Buf, BufLength);
    FreeMemory(Buf, BufLength);
    CloseSystemHandle(&BuildFile.Handle);
    FreeString(&FullBuildFileName);

    GetJsonString(&ActualBuildDirectory, BuildFileObject, "build_path");

    // If the build file points to another build file make sure to use the build file with the actual build
    // information
    if (ActualBuildDirectory.Capacity > 0)
    {
        if (EndsWith(ActualBuildDirectory, CreateString("/")))
        {
            FormatString(&FullBuildFileName, "%str%str", ActualBuildDirectory, BuildFileName);
        }
        else
        {
            FormatString(&FullBuildFileName, "%str/%str", ActualBuildDirectory, BuildFileName);
        }

        BuildFile = OpenFile(FullBuildFileName, FILE_READ, false);
        Buf       = ReadFile(BuildFile);
        BufLength = GetFileLength(FullBuildFileName);

        DestroyJson(BuildFileObject);
        BufferToJSON(BuildFileObject, Buf, BufLength);
        FreeMemory(Buf, BufLength);
        CloseSystemHandle(&BuildFile.Handle);
        FreeString(&FullBuildFileName);

        string InitialDirectoryConst = CreateString("initial_directory");

        if (ActualBuildDirectory[0] == '/')
        {
            AddJsonString(BuildFileObject, InitialDirectoryConst, ActualBuildDirectory);
        }
        else
        {
            string WorkingDirectory;

            GetCurrentWorkingDirectory(&WorkingDirectory);
            FormatString(&ActualBuildDirectory, "%str/%str", WorkingDirectory, ActualBuildDirectory);
            AddJsonString(BuildFileObject, InitialDirectoryConst, ActualBuildDirectory);
            FreeString(&WorkingDirectory);
        }
    }

    return (true);
}

b32
ParseBuildFile(build_file* BuildFile, string BuildFileName)
{
    engine_memset((char*)BuildFile, 0, sizeof(build_file));
    GetCurrentWorkingDirectory(&BuildFile->InitialDirectory);

    if (!ReadRealBuildFile(&BuildFile->BuildFileObject, BuildFileName, BuildFile->InitialDirectory))
    {
        string Message;

        FormatString(&Message, "The Build File could not be read from %str/%str\n",
                     BuildFile->InitialDirectory, BuildFileName);
        StandardPrint(Message);
        FreeString(&Message);

        return (false);
    }

    string InitialDirectoryConst = CreateString("initial_directory");
    string NewInitialDirectory;

    CreateArray(&BuildFile->Builds);

    if (GetJsonString(&NewInitialDirectory, &BuildFile->BuildFileObject, InitialDirectoryConst))
    {
        FreeString(&BuildFile->InitialDirectory);
        CopyString(&BuildFile->InitialDirectory, NewInitialDirectory);
    }

    array<json> BuildObjects      = BuildFile->BuildFileObject.Objects;
    u32         BuildObjectsCount = BuildObjects.Size;

    for (u32 BuildObjectIndex = 0; BuildObjectIndex < BuildObjectsCount; ++BuildObjectIndex)
    {
        build* NewBuild = Add(&BuildFile->Builds);

        NewBuild->BuildObject = BuildObjects + BuildObjectIndex;
        NewBuild->BaseFolder  = BuildFile->InitialDirectory;
    }

    return (true);
}

void
DestroyBuildFile(build_file* BuildFile)
{
    DestroyBuilds(&BuildFile->Builds);

    DestroyJson(&BuildFile->BuildFileObject);
    FreeArray(&BuildFile->Builds);
    FreeString(&BuildFile->InitialDirectory);
}
