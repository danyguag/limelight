#if !defined(PLATFORM_H)

#include <dlib1/array.h>
#include <dlib1/string.h>
#include <dlib1/types.h>

void CallExecutablesAsync(string CompilerPath, array<array<string>>* CommandFlags, bool wait);
void PrintTimer_(string Name, s64 StartTime, s64 EndTime);
b32  CopyFile(string& Source, string& Destination);
b32  SetExecutable(string& DestinationFile);
void GetUserHomeDirectory(string* Out);
b32  CallExecutable(string Path, array<string>* Args);

#define PrintTimer(Name, StartTime) PrintTimer_(Name, StartTime, GetTime())

#define PLATFORM_H
#endif
