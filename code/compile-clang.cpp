#include "compile-ar.h"
#include "compile-gpp.h"
#include "platform.h"

#include <dlib1/base.h>

DESTROY_COMPILATION_USERDATA(DestroyCompilationUserDataClang);
CREATE_LIBRARY_STATIC(CreateLibraryStaticClang);
CREATE_LIBRARY_SHARED(CreateLibrarySharedClang);
CREATE_EXECUTABLE(CreateExecutableClang);

static void CreateBaseFlags(array<string>* BaseFlags, build_type BuildType);

struct clang_build_data
{
    array<string> BaseFlags;
    array<string> ObjectFileNames;
};

void
GetClangCompilationProcess(build* Build)
{
    clang_build_data* BuildData = (clang_build_data*)AllocateMemory(sizeof(clang_build_data));

    CreateBaseFlags(&BuildData->BaseFlags, Build->BuildType);
    CreateArray(&BuildData->ObjectFileNames);

    Build->DestroyCompilationUserData = DestroyCompilationUserDataClang;
    Build->CreateLibraryStatic        = CreateLibraryStaticClang;
    Build->CreateLibraryShared        = CreateLibrarySharedClang;
    Build->CreateExecutable           = CreateExecutableClang;
    Build->UserData                   = BuildData;
}

void
DestroyCompilationUserDataClang(void* UserData)
{
    clang_build_data* BuildData = (clang_build_data*)UserData;

    for (u64 BaseFlagIndex = 0; BaseFlagIndex < BuildData->BaseFlags.Size; ++BaseFlagIndex)
    {
        FreeString(BuildData->BaseFlags + BaseFlagIndex);
    }

    for (u64 ObjectFileNameIndex = 0; ObjectFileNameIndex < BuildData->ObjectFileNames.Size;
         ++ObjectFileNameIndex)
    {
        FreeString(BuildData->ObjectFileNames + ObjectFileNameIndex);
    }

    FreeArray(&BuildData->BaseFlags);
    FreeArray(&BuildData->ObjectFileNames);
    FreeMemory((char*)UserData, sizeof(clang_build_data));
}

b32
CreateCompilationObjectsClang(build* Build, clang_build_data* BuildData)
{
    array<array<string>> ObjectFileArgs;
    string               CompilerPath             = CreateString("/usr/bin/clang");
    u32                  InputFileCount           = Build->InputFiles.Size;
    u64                  BuildObjectFileNameCount = BuildData->ObjectFileNames.Size;

    CreateArray(&ObjectFileArgs);

    for (u32 FileIndex = 0; FileIndex < InputFileCount; ++FileIndex)
    {
        comp_file* CurrentBuildFile = Build->InputFiles + FileIndex;
        string     ObjectFileName   = CurrentBuildFile->ObjectFileName;

        array<string> CurrentObjectFileArgs;
        CreateArray(&CurrentObjectFileArgs);
        Add(&CurrentObjectFileArgs, &BuildData->BaseFlags);
        Add(&CurrentObjectFileArgs, CreateString("-c"));
        Add(&CurrentObjectFileArgs, CreateString("-fPIC"));
        Add(&CurrentObjectFileArgs, CurrentBuildFile->FullName);

        if (Build->ExtraFlags.AllocatedLength > 0)
        {
            Add(&CurrentObjectFileArgs, &Build->ExtraFlags);
        }

        if (Build->Dependencies.IncludeStrings.AllocatedLength > 0)
        {
            Add(&CurrentObjectFileArgs, &Build->Dependencies.IncludeStrings);
        }

        GenerateCompileCommand(&CurrentBuildFile->Command, CompilerPath, &CurrentObjectFileArgs);

        Add(&BuildData->ObjectFileNames, ObjectFileName);
        Add(&ObjectFileArgs, CurrentObjectFileArgs);
    }

    // Execute all of the commands at one time on different threads
    CallExecutablesAsync(CompilerPath, &ObjectFileArgs, true);

    b32 Result = true;

    for (u64 CommandIndex = 0; CommandIndex < ObjectFileArgs.Size; ++CommandIndex)
    {
        array<string>* Args = ObjectFileArgs + CommandIndex;

        for (u64 ArgIndex = 0; ArgIndex < Args->Size; ++ArgIndex)
        {
            FreeString(GetElement(Args, ArgIndex));
        }

        FreeArray(Args);
    }

    for (u64 ObjectFileNameIndex = BuildObjectFileNameCount;
         ObjectFileNameIndex < BuildData->ObjectFileNames.Size; ++ObjectFileNameIndex)
    {
        string& ObjectFileName = BuildData->ObjectFileNames[ObjectFileNameIndex];

        if (!Result && !DoesFileExist(ObjectFileName))
        {
            Result = false;
	    break;
        }
    }

    FreeArray(&ObjectFileArgs);

    return (Result);
}

b32
CreateLibraryStaticClang(build* Build, void* UserData)
{
    clang_build_data* BuildData = (clang_build_data*)UserData;

    if (!CreateCompilationObjectsClang(Build, BuildData))
    {
        return (false);
    }

    if (Build->Dependencies.LibraryStrings.AllocatedLength > 0)
    {
        Add(&BuildData->ObjectFileNames, &Build->Dependencies.LibraryStrings);
    }

    string LibraryName;

    FormatString(&LibraryName, "lib%str.a", Build->ExecutableName);
    CreateStaticLibraryAr(LibraryName, NULL, &BuildData->ObjectFileNames);

    b32 Result = DoesFileExist(LibraryName);

    FreeString(&LibraryName);

    return (Result);
}

b32
CreateLibrarySharedClang(build* Build, void* UserData)
{
    array<string>     Flags;
    clang_build_data* BuildData    = (clang_build_data*)UserData;
    string            CompilerPath = CreateString("/usr/bin/clang");

    CreateArray(&Flags);
    Add(&Flags, CreateString("-pthread"));
    Add(&Flags, CreateString("-shared"));
    Add(&Flags, CreateString("-fPIC"));
    Add(&Flags, CreateString("-o"));

    string* LibraryName = Add(&Flags);

    FormatString(LibraryName, "%str.so", Build->ExecutableName);

    for (u64 InputFileIndex = 0; InputFileIndex < Build->InputFiles.Size; ++InputFileIndex)
    {
        Add(&Flags, Build->InputFiles[InputFileIndex].FullName);
    }

    if (Build->Dependencies.LibraryStrings.AllocatedLength > 0)
    {
        Add(&Flags, &Build->Dependencies.LibraryStrings);
    }

    Add(&Flags, &BuildData->ObjectFileNames);

    if (Build->ExtraFlags.AllocatedLength > 0)
    {
        Add(&Flags, &Build->ExtraFlags);
    }

    CallExecutable(CompilerPath, &Flags);
    b32 Result = DoesFileExist(*LibraryName);

    for (u64 FlagIndex = 0; FlagIndex < Flags.Size; ++FlagIndex)
    {
        FreeString(Flags + FlagIndex);
    }

    FreeArray(&Flags);

    return (Result);
}

b32
CreateExecutableClang(build* Build, void* UserData)
{
    array<string>     Flags;
    clang_build_data* BuildData    = (clang_build_data*)UserData;
    string            CompilerPath = CreateString("/usr/bin/g++");

    if (!CreateCompilationObjectsClang(Build, BuildData))
    {
        return (false);
    }

    CreateArray(&Flags);
    Add(&Flags, &BuildData->BaseFlags);
    Add(&Flags, CreateString("-o"));
    Add(&Flags, Build->ExecutableName);
    Add(&Flags, Build->InputFile.FullName);

    if (Build->Dependencies.IncludeStrings.AllocatedLength > 0)
    {
        Add(&Flags, &Build->Dependencies.IncludeStrings);
    }

    if (Build->InputFiles.Size > 0)
    {
        Add(&Flags, &BuildData->ObjectFileNames);
    }

    if (Build->Dependencies.LibraryStrings.AllocatedLength > 0)
    {
        Add(&Flags, &Build->Dependencies.LibraryStrings);
    }

    if (Build->ExtraFlags.AllocatedLength > 0)
    {
        Add(&Flags, &Build->ExtraFlags);
    }

    GenerateCompileCommand(&Build->InputFile.Command, CompilerPath, &Flags);

    CallExecutable(CompilerPath, &Flags);

    return DoesFileExist(Build->ExecutableName);
}

static void
CreateBaseFlags(array<string>* BaseFlags, build_type BuildType)
{
    CreateArray(BaseFlags);

    Add(BaseFlags, CreateString("-pthread"));
    Add(BaseFlags, CreateString("-std=c++17"));
    Add(BaseFlags, CreateString("-DPLATFORM_LINUX"));
    Add(BaseFlags, CreateString("-fno-exceptions"));

    if (BuildType == BUILD_TYPE_DEBUG)
    {
        Add(BaseFlags, CreateString("-g"));
        Add(BaseFlags, CreateString("-O0"));
    }
    else if (BuildType == BUILD_TYPE_RELEASE)
    {
        Add(BaseFlags, CreateString("-O3"));
    }
}
