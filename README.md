# Limelight Build System

This project is a build system and a package manager.  This can use gcc, clang, make, or any other compilation system.  For more information about build configurations please visit the wiki on the gitlab.  As of know the only subcommand that works is the build command.  Use it as follows:

```sh
limelight build
```

As the package manager is implemented, tested, and released the package manager will be invoked as followed:

```sh
limelight install <package name> # to install a package
limelight update # to update all installed packages and the package list
limelight publish <package name> # to install a package
```
More documentation about each command is found on the wiki.
